﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DTGV.Services;

namespace DTGV
{
    public partial class Lop1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                
                if (Session["LoginType"].ToString() != "2")
                {
                    Response.Redirect("Notpermission.aspx");
                }
                string _so = Session["Donvi"].ToString();
                //int quyen = int.Parse(Session["LoginType"].ToString());
                var listLop = new LopService().DanhsachLop(_so);
                drgLop.DataSource = listLop;
                drgLop.DataBind();

            }


        }
        private bool validData()
        {
            if (string.IsNullOrEmpty(txtMalop.Text))
            {
                lblErr.Text = "nhập lại mã lớp";
                return false;
            }
            if (string.IsNullOrEmpty(txtTrangthai.Text))
            {
                lblErr.Text = "Nhập lại trạng thái";
                return false;
            }
            if (string.IsNullOrEmpty(txtDiadiem.Text))
            {
                lblErr.Text = "Nhập lại địa điểm";
                return false;
            }
            if (string.IsNullOrEmpty(txtTenlop.Text))
            {
                lblErr.Text = "Nhập lại tên lớp";
                return false;
            }

            return true;
        }
        protected void drgLop_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            drgLop.PageIndex = e.NewPageIndex;
            drgLop.DataBind();
        }
        protected void btnTimKiem_Click(object sender, EventArgs e)
        {
            if ((string.IsNullOrEmpty(txtDiadiem.Text)) && (string.IsNullOrEmpty(txtTrangthai.Text)) && (string.IsNullOrEmpty(txtMalop.Text)) && (string.IsNullOrEmpty(txtTenlop.Text)))
            {
                string _so = Session["Donvi"].ToString();
                var listLop = new LopService().DanhsachLop(_so);
                drgLop.DataSource = listLop;
                drgLop.DataBind();
            }
            else
            {
                var listlop = new LopService().TimKiem(txtTenlop.Text, txtMalop.Text, txtDiadiem.Text, txtTrangthai.Text);
                drgLop.DataSource = listlop;
                drgLop.DataBind();
            }

        }
        protected void btnUpdateLop_Command(object sender, CommandEventArgs e)
        {
            var malop = e.CommandArgument.ToString();
            Response.Redirect("LopEdit.aspx?malop=" + malop);
        }
        protected void btnAddDaotao_Command(object sender, CommandEventArgs e)
        {
            var malop = e.CommandArgument.ToString();
            Response.Redirect("LopAddDaotao.aspx?malop=" + malop);
        }
        protected void btnLopView_Command(object sender, CommandEventArgs e)
        {
            var malop = e.CommandArgument.ToString();
            Response.Redirect("LopViewDSDT.aspx?malop=" + malop);
        }
        protected void btnAddLop_Click(object sender, EventArgs e)
        {
            Response.Redirect("LopAdd.aspx");
        }
    }
}