﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DTGV.Services;

namespace DTGV
{
    public partial class Taikhoan1 : System.Web.UI.Page
    {
        UtilityService usv = new UtilityService();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["LoginType"].ToString() != "1")
                {
                    Response.Redirect("Notpermission.aspx");
                }
                var listTaikhoan = new TaikhoanService().DanhsachTaikhoan();
                drgTaikhoan.DataSource = listTaikhoan;
                drgTaikhoan.DataBind();
            }
        }
        private bool validData()
        {
            if (string.IsNullOrEmpty(txtHoten.Text))
            {
                lblErr.Text = "Bạn chưa nhập họ tên";
                return false;
            }
            if (string.IsNullOrEmpty(txtID.Text))
            {
                lblErr.Text = "Bạn chưa nhập tên đăng nhập";
                return false;
            }
            if (string.IsNullOrEmpty(txtPassword.Text))
            {
                lblErr.Text = "Bạn chưa nhập mật khẩu";
                return false;
            }
            if (ddlQuyen.SelectedValue == "0")
            {
                lblErr.Text = "Bạn cần lựa chọn quyền";
                return false;
            }

            return true;
        }
        protected void txtThemmoi_Click(object sender, EventArgs e)
        {
            if (!validData())
            {
                return;
            }
            else
            {
                Taikhoan dt = new Taikhoan();
                dt.Username = txtID.Text;
                dt.Password = usv.Md5Gen(txtPassword.Text);
                dt.Hoten = txtHoten.Text;
                dt.Donvicongtac = txtDonvicongtac.Text;
                dt.Quyen = int.Parse(ddlQuyen.SelectedValue.ToString());
                dt.Ghichu = txtGhichu.Text;
                TaikhoanService svc = new TaikhoanService();
                svc.InsertTaikhoan(dt);
                var listTaikhoan = new TaikhoanService().DanhsachTaikhoan();
                drgTaikhoan.DataSource = listTaikhoan;
                drgTaikhoan.DataBind();
            }

        }

        protected void btnTimKiem_Click(object sender, EventArgs e)
        {
            if ((string.IsNullOrEmpty(txtID.Text)) && (string.IsNullOrEmpty(txtPassword.Text)) && (string.IsNullOrEmpty(txtHoten.Text)) && (string.IsNullOrEmpty(txtDonvicongtac.Text)))
            {
                var listQD = new TaikhoanService().DanhsachTaikhoan();
                drgTaikhoan.DataSource = listQD;
                drgTaikhoan.DataBind();
            }
            else
            {
                var listQD = new TaikhoanService().TimKiem(txtID.Text, usv.Md5Gen(txtPassword.Text), txtHoten.Text, txtDonvicongtac.Text);
                drgTaikhoan.DataSource = listQD;
                drgTaikhoan.DataBind();
            }
        }
        protected void btnUpdate_Command(object sender, CommandEventArgs e)
        {
            //int quyen = int.Parse(drgTaikhoan.SelectedRow.Cells[4].Text);
            //GridViewRow gvr = (GridViewRow)(((Label)e.CommandName).NamingContainer);
            //string _quyen = (gvr.FindControl("lblquyen") as Label).Text;
            //int _quyen1 = int.Parse(_quyen);
            
            //if(_quyen1!=1)
            //{
                var _username = e.CommandArgument.ToString();

                Response.Redirect("TaikhoanEdit.aspx?username=" + _username);
            //}
            //else
            //{
            //    Response.Redirect("Notpermission.aspx");
            //}
            

        }

        protected void drgTaikhoan_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            drgTaikhoan.PageIndex = e.NewPageIndex;
            drgTaikhoan.DataBind();
        }
    }
}