﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DTGV.Services;

namespace DTGV
{
    public partial class Login : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnLogin_Click(object sender, EventArgs e)
        {
            AcountService svc = new AcountService();
            var user = svc.CheckSignin(txtUsername.Text.Trim(), txtPassword.Text.Trim());
            if (user != null)
            {
                Session["Username"] = user.Username;
                Session["LoginType"] = user.Quyen;
                Session["Name"] = user.Hoten;
                Session["Avatar"] = user.Avatar;
                Session["Donvi"] = user.Donvicongtac;
                var returnUrl = Request.QueryString["ReturnUrl"];
                if(!string.IsNullOrWhiteSpace(returnUrl))
                    Response.Redirect(returnUrl);
                Response.Redirect("Daotao.aspx");

            }
            else
            {
                Response.Write("Đăng nhập không thành công");
            }

        }

        protected void btnLogin_Click1(object sender, EventArgs e)
        {

        }
    }
}
