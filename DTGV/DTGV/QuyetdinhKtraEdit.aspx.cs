﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DTGV.Services;

namespace DTGV
{
    public partial class QuyetdinhEdit : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            var _soquyetdinh = Request.QueryString["soquyetdinh"].ToString();
            txtsoQD.Text = _soquyetdinh;
        }

        protected void btnLuu_Click(object sender, EventArgs e)
        {
            
        }

        protected void btHuy1_Click(object sender, EventArgs e)
        {

        }

        protected void btnLuu_Click1(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                var _soquyetdinh = Request.QueryString["soquyetdinh"].ToString();
                QuyetdinhKTra dt = new QuyetdinhKTra();
                dt.Soquyetdinh = int.Parse(_soquyetdinh);
                dt.Ngaylap = bdpNgaylap.SelectedDate;
                dt.Ngaybatdau = bdpNgaybatdau.SelectedDate;
                dt.Ngayketthuc = bdpNgayketthuc.SelectedDate;
                if (DateTime.Compare(bdpNgaybatdau.SelectedDate, DateTime.Now) == -1)
                {
                    dt.Trangthai = "Chưa bắt đầu";
                }
                else
                {
                    if (DateTime.Compare(bdpNgayketthuc.SelectedDate, DateTime.Now) == 1)
                    {
                        dt.Trangthai = "Đang diễn ra";
                    }
                    else
                    {
                        dt.Trangthai = "Đã kết thúc";
                    }
                }
                
                dt.Diadiem = txtDiadiem.Text;
                dt.Malop = txtMalop.Text;
                dt.Noidung = txtNoidung.Text;
                dt.Ghichu = txtGhichu.Text;
                QuyetdinhKtraService svc = new QuyetdinhKtraService();
                svc.UpdateQuyetdinh(dt);
            }
        }

        protected void btHuy1_Click1(object sender, EventArgs e)
        {
            Response.Redirect("QuyetdinhKtra.aspx");
        }
    }
}