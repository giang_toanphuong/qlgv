﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DTGV.Services;

namespace DTGV
{
    public partial class QuyetdinhAdd : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnLuu_Click(object sender, EventArgs e)
        {
            QuyetdinhKTra dt = new QuyetdinhKTra();

            dt.Soquyetdinh = int.Parse(txtsoQD.Text);

            dt.Ngaylap = bdpNgaylap.SelectedDate;
            dt.Ngaybatdau = bdpNgaybatdau.SelectedDate;
            dt.Ngayketthuc = bdpNgayketthuc.SelectedDate;
            dt.Trangthai = txtTrangthai.Text;
            dt.Diadiem = txtDiadiem.Text;
            dt.Malop = txtMalop.Text;
            dt.Noidung = txtNoidung.Text;
            dt.Ghichu = txtGhichu.Text;
            QuyetdinhKtraService svc = new QuyetdinhKtraService();
            svc.InsertQuyetdinh(dt);
        }

        protected void btHuy1_Click(object sender, EventArgs e)
        {
            Response.Redirect("QuyetdinhKtra.aspx");
        }
    }
}