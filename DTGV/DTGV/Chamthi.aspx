﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Chamthi.aspx.cs" Inherits="DTGV.Chamthi" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4>Thông tin tìm kiếm</h4>
        </div>
        <div class="panel-body panel-body-nopadding">
            <div class="form-horizontal form-striped">
                <br />
                <div class="form-group">
                    <label class="col-sm-2 control-label text-right">Tên lớp</label>

                    <div class="col-sm-4">
                        <asp:TextBox runat="server" ID="txtTenlop" CssClass="form-control"></asp:TextBox>
                    </div>
                    <label class="col-sm-2 control-label text-right">Mã lớp</label>

                    <div class="col-sm-4">
                        <asp:TextBox runat="server" ID="txtMalop" CssClass="form-control"></asp:TextBox>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-md-10 text-right">
                        <asp:Button ID="btnTimKiem" runat="server" CssClass="btn btn-primary" Text="Tìm kiếm" OnClick="btnTimKiem_Click" />
                    </div>


                </div>
            </div>
        </div>
        <br />
    </div>
    <asp:Label ID="lblMessage" runat="server" Text=""></asp:Label>
    <div class="col-sm-8 text-left">
        <h3>
            <asp:Label ID="Label1" runat="server" Text=""></asp:Label>

        </h3>
        <h5>
            <asp:Label ID="Label2" runat="server" Text=""></asp:Label>

        </h5>
    </div>
    <div class="table-responsive" style="text-align: center">
        <%-- <asp:GridView ID="drgHoso" AllowPaging="True" PageSize="10" runat="server" CssClass="table table-striped" AutoGenerateColumns="False"   OnPageIndexChanging="drgHoso_PageIndexChanging">--%>
        <asp:GridView ID="drgtest" AllowPaging="true" PageSize="10" runat="server"  CssClass="table table-striped" AutoGenerateColumns="False" OnPageIndexChanging="drgtest_PageIndexChanging">
            <Columns>

                <asp:TemplateField HeaderText="Họ tên">
                    <ItemTemplate><%# Eval("Hoten") %></ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Số chứng minh nhân dân">
                    <ItemTemplate>
                        <asp:Label ID="lblCmnd" runat="server" Text='<%# Eval("Cmnd") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Số GPLX">
                    <ItemTemplate><%# Eval("GPLX") %></ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Vắng thi">
                    <ItemTemplate>
                        <asp:CheckBox ID="chkVangthi" runat="server" />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Văn bản">
                    <ItemTemplate>
                        <asp:CheckBox ID="chkVanban" runat="server" />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Lý thuyết">
                    <ItemTemplate>
                        <asp:CheckBox ID="chkLythuyet" runat="server" />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="TH Trong hình">
                    <ItemTemplate>
                        <rowstyle horizontalalign="right"></rowstyle>
                        <asp:CheckBox ID="chkThuchanhtronghinh" runat="server" />
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField HeaderText="TH Báo hình">
                    <ItemTemplate>
                        <asp:CheckBox ID="chkThuchanhbaohinh" runat="server" />
                    </ItemTemplate>
                </asp:TemplateField>

                <%--<asp:TemplateField HeaderText="Kết quả">
                    <ItemTemplate>
                        <asp:Label ID="lbketqua" runat="server" />
                    </ItemTemplate>
                </asp:TemplateField>--%>
                <%--<asp:CommandField EditText="Sửa" CancelText="Huỷ bỏ" UpdateText="Cập nhật" ShowEditButton="True" AccessibleHeaderText="Sửa" />
                <asp:CommandField DeleteText="Xoá" ShowDeleteButton="True" AccessibleHeaderText="Xoá" />--%>
            </Columns>
        </asp:GridView>




    </div>
    <div class="form-group">
        <div class="col-md-6 text-right">
            <asp:Button ID="btnLuuDL" runat="server" CssClass="btn btn-primary" Text="Lưu Dữ liệu" OnClick="btnLuuDL_Click" />
        </div>
        <div class="col-md-2 text-right">
            <asp:Button ID="btnDSDo" runat="server" CssClass="btn btn-primary" Text="In DS Đỗ" OnClick="btnDSDo_Click" />
        </div>
        <div class="col-md-2 text-right">
            <asp:Button ID="btnDSTruot" runat="server" CssClass="btn btn-primary" Text="In DS trượt" OnClick="btnDSTruot_Click" />
        </div>
        <div class="col-md-2 text-right">
            <asp:Button ID="btnDSvangthi" runat="server" CssClass="btn btn-primary" Text="In DS vắng thi" OnClick="btnDSvangthi_Click" />
        </div>

    </div>


    <div class="col-sm-8 text-left">
        <h3>
            <asp:Label ID="txtInDS" runat="server" Text=""></asp:Label>

        </h3>

    </div>
    <div class="table-responsive" style="text-align: center">

        <asp:GridView ID="drgInDS" AllowPaging="true" PageSize="10" runat="server" CssClass="table table-striped" AutoGenerateColumns="False" OnPageIndexChanging="drgInDS_PageIndexChanging">
            <Columns>
                <asp:TemplateField HeaderText="Họ tên">
                    <ItemTemplate><%# Eval("Hoten") %></ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Giới tính">
                    <ItemTemplate><%# Eval("Gioitinh") %></ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Ngày sinh">
                    <ItemTemplate><%# Eval("Ngaysinh") %></ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="CMND">
                    <ItemTemplate><%# Eval("Cmnd") %></ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Số GPLX">
                    <ItemTemplate><%# Eval("GPLX") %></ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Ngày cấp">
                    <ItemTemplate><%# Eval("NgaycapGPLX") %></ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Ngày hết hạn">
                    <ItemTemplate><%# Eval("NgayhethanGPLX") %></ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Văn hoá">
                    <ItemTemplate><%# Eval("Vanhoa") %></ItemTemplate>
                </asp:TemplateField>
            </Columns>
        </asp:GridView>

    </div>
    <div class="form-group">
        <div class="col-md-6 text-right">
            <asp:Button ID="btnXuatDL" runat="server" CssClass="btn btn-primary" Text="Xuất dữ liệu" OnClick="btnXuatDL_Click" />
        </div>
    </div>
</asp:Content>
