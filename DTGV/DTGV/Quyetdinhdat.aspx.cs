﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DTGV.Services;
using System.Data;
using System.Configuration;
using System.Data.SqlClient;

namespace DTGV
{
    public partial class Quyetdinhdat : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {

                if (Session["LoginType"].ToString() != "2")
                {
                    Response.Redirect("Notpermission.aspx");
                }
                //chen dl từ csdl vào dropdownlist
                //using (var db = new DTGVDataContext())
                //{

                //}
                //    string constr = ConfigurationManager.ConnectionStrings["constr"].ConnectionString;
                //using (SqlConnection con = new SqlConnection(constr))
                //{
                //    using (SqlCommand cmd = new SqlCommand("SELECT Malop, Tenlop FROM Lop"))
                //    {
                //        cmd.CommandType = CommandType.Text;
                //        cmd.Connection = con;
                //        con.Open();
                //        ddlMalop.DataSource = cmd.ExecuteReader();
                //        ddlMalop.DataTextField = "Tenlop";
                //        ddlMalop.DataValueField = "Malop";
                //        ddlMalop.DataBind();
                //        con.Close();
                //    }
                //}
                //ddlMalop.Items.Insert(0, new ListItem("--Select Lop--", "0"));

                var listLop = new LopService().DanhsachLop(Session["Donvi"].ToString());
                ddlMalop.DataSource = listLop;
                ddlMalop.DataTextField = "Tenlop";
                ddlMalop.DataValueField = "Malop";
                ddlMalop.DataBind();
                string _so = Session["Donvi"].ToString();
                //int quyen = int.Parse(Session["LoginType"].ToString());
                var listQD = new QuyetdinhDatService().DanhSachQuyetdinh();
                drgQDDat.DataSource = listQD;
                drgQDDat.DataBind();
                //var listLop = new LopService().DanhsachLop(_so);
                //drgLop.DataSource = listLop;
                //drgLop.DataBind();

            }
        }
        private bool validData()
        {
            if (string.IsNullOrEmpty(txtSoQuyetDinh.Text))
            {
                lblErr.Text = "Nhập lại số quyết định";
                return false;
            }
            
            //if (string.IsNullOrEmpty(txtMalop.Text))
            //{
            //    lblErr.Text = "Nhập mã lớp";
            //    return false;
            //}

            return true;
        }
        protected void drgQDDat_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            drgQDDat.PageIndex = e.NewPageIndex;
            drgQDDat.DataBind();
        }
        protected void btnLuu_Click(object sender, EventArgs e)
        {

        }

        protected void btHuy1_Click(object sender, EventArgs e)
        {

        }

        protected void btnTimKiem_Click(object sender, EventArgs e)
        {
            //if ((string.IsNullOrEmpty(txtSoQuyetDinh.Text))&& (string.IsNullOrEmpty(txtMalop.Text)) )
            if ((string.IsNullOrEmpty(txtSoQuyetDinh.Text)) && (ddlMalop.SelectedValue=="0"))
            {
                var listQD = new QuyetdinhDatService().DanhSachQuyetdinh();
                drgQDDat.DataSource = listQD;
                drgQDDat.DataBind();
            }
            else
            {
                var listQD = new QuyetdinhDatService().TimKiem(int.Parse(txtSoQuyetDinh.Text),  ddlMalop.SelectedValue);
                drgQDDat.DataSource = listQD;
                drgQDDat.DataBind();
            }
        }
        protected void btnEdit_Command(object sender, CommandEventArgs e)
        {
            var _soquyetdinh = e.CommandArgument.ToString();

            Response.Redirect("QuyetdinhDatEdit.aspx?soquyetdinh=" + _soquyetdinh);

        }
        protected void btnAddDaotao_Command(object sender, CommandEventArgs e)
        {
            //string[] _soquyetdinh = e.CommandArgument.ToString().Split(',');
            //string soquyetdinh = "";
            //DateTime ngaylap = DateTime.Now;
            //if (_soquyetdinh.Length > 0)
            //{
            //    soquyetdinh = _soquyetdinh[0];
            //    ngaylap = DateTime.Parse(_soquyetdinh[1]);
            //}

            //Response.Redirect("QuyetdinhDatAddDaotao.aspx?soquyetdinh=" + soquyetdinh+ "&ngaylap="+ngaylap);

            var _soquyetdinh = e.CommandArgument.ToString();
            Response.Redirect("QuyetdinhDatAddDaotao.aspx?soquyetdinh" + _soquyetdinh);
        }
        protected void btnThemQD_Click(object sender, EventArgs e)
        {
            Response.Redirect("QuyetdinhDatAdd.aspx");

        }
    }
}