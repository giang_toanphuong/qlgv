﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Giaovien.aspx.cs" Inherits="DTGV.Giaovien1" %>
<%@ Register Assembly="BasicFrame.WebControls.BasicDatePicker" Namespace="BasicFrame.WebControls" TagPrefix="BDP" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
     <div class="panel panel-default">
        <div class="panel-heading">
            <h3>Thông tin tìm kiếm</h3>
        </div>
        <div class="panel-body panel-body-nopadding">
            <div class="danger">
                <asp:Label ID="lblErr" runat="server"></asp:Label>
            </div>
            <form class="form-horizontal form-striped">
                <br />
                <div class="form-group">
                    <label class="col-sm-2 control-label text-right">Họ và tên</label>
                    <div class="col-sm-4">
                        <asp:TextBox runat="server" ID="txtHoten" CssClass="form-control"></asp:TextBox>
                    </div>
                    <label class="col-sm-2 control-label text-right">Số CMND</label>
                    <div class="col-sm-4">
                        <asp:TextBox runat="server" ID="txtCmnd" CssClass="form-control"></asp:TextBox>
                    </div>
                    

                </div>


                

                <div class="form-group">
                    <label class="col-sm-2 control-label text-right">Số GPLX</label>
                    <div class="col-sm-4">
                        <asp:TextBox runat="server" ID="txtGPLX" CssClass="form-control"></asp:TextBox>
                    </div>
                    <label class="col-sm-2 control-label text-right">Trình độ</label>
                    <div class="col-sm-4">
                        <asp:TextBox runat="server" ID="txtVanhoa" CssClass="form-control"></asp:TextBox>
                    </div>

                </div>
                <div class="form-group">
                    <div class="col-md-10 text-right">
                        <asp:Button ID="btnTimKiem" runat="server" CssClass="btn btn-primary" Text="Tìm kiếm" OnClick="btnTimKiem_Click"  />
                    </div>

                </div>
                <br />
            </form>
        </div>
    </div>
    <div class="col-sm-8 text-left">
        <h3>Danh sách Giáo viên</h3>
    </div>

    <div class="table-responsive">

        <asp:GridView ID="drgGV" AllowPaging="true" PageSize="2" runat="server" CssClass="table table-striped" AutoGenerateColumns="False" OnPageIndexChanging="drgGV_PageIndexChanging">
            <Columns>
                <asp:TemplateField HeaderText="Họ tên">
                    <ItemTemplate><%# Eval("Hoten") %></ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Giới tính">
                    <ItemTemplate><%# Eval("Gioitinh") %></ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Ngày sinh">
                    <ItemTemplate><%# Eval("Ngaysinh") %></ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Số CMND">
                    <ItemTemplate><%# Eval("Cmnd") %></ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Số bằng">
                    <ItemTemplate><%# Eval("GPLX") %></ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Văn hoá">
                    <ItemTemplate><%# Eval("Vanhoa") %></ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Quê quán">
                    <ItemTemplate><%# Eval("Que") %></ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Chọn">
                    <ItemTemplate>
                        <asp:CheckBox ID="chkChon" runat="server" />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Hiển thị">
                    <ItemTemplate>
                        <asp:LinkButton runat="server" Text="Hiển Thị" ID="btnView" CommandArgument='<%# Eval("cmnd")%> ' OnCommand="btnView_Command"></asp:LinkButton>
                    </ItemTemplate>
                </asp:TemplateField>
                <%--<asp:CommandField EditText="Sửa" CancelText="Huỷ bỏ" UpdateText="Cập nhật" ShowEditButton="True" AccessibleHeaderText="Sửa" />
                <asp:CommandField DeleteText="Xoá" ShowDeleteButton="True" AccessibleHeaderText="Xoá" />--%>
            </Columns>
        </asp:GridView>


    </div>
    <div class="form-group">
        <div class="col-md-10 text-right">
            <asp:Button ID="btnInDSGV" runat="server" CssClass="btn btn-primary" Text="In Danh sách" />
        </div>
        <div class="col-md-1 text-center">
            <asp:Button ID="btnThongkeGV" runat="server" CssClass="btn btn-primary" Text="Thống kê" />
        </div>
    </div>
</asp:Content>
