﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(DTGV.Startup))]
namespace DTGV
{
    public partial class Startup {
        public void Configuration(IAppBuilder app) {
            ConfigureAuth(app);
        }
    }
}
