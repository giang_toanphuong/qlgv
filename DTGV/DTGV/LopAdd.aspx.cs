﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DTGV.Services;

namespace DTGV
{
    public partial class LopAdd : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        //private bool CompareTime(DateTime _datetime)
        //{
        //    DateTime myDate = DateTime.Now;
        //    string current = myDate.ToString("MM/dd/yyyy");             
        //    string picker = _datetime.ToString("MM/dd/yyyy");
        //    if (picker > current)
        //    {
        //        return true
        //    }
        //    else
        //    {
        //        textBox1.Text = "khac nhau";
        //    }
        //}
        protected void btnLuu_Click(object sender, EventArgs e)
        {
            Lop dt = new Lop();
            dt.Tenlop = txtTenlop.Text;
            dt.Malop = txtMalop.Text;
            dt.Soluong = int.Parse(txtSoluong.Text);
            dt.Ngaylap = bdpNgaylap.SelectedDate;
            dt.Ngaybatdau = bdpNgaybatdau.SelectedDate;
            dt.Ngayketthuc = bdpNgayketthuc.SelectedDate;
            dt.Diadiem = txtDiadiem.Text;
            dt.Ghichu = txtGhichu.Text;
            if(DateTime.Compare(bdpNgaybatdau.SelectedDate,DateTime.Now)==-1)
            {
                dt.Trangthai = "Chưa bắt đầu";
            }
            else
            {
                if (DateTime.Compare(bdpNgayketthuc.SelectedDate, DateTime.Now) == -1)
                {
                    dt.Trangthai = "Đang diễn ra";
                }
                else
                {
                    dt.Trangthai = "Đã kết thúc";
                }
            }
            LopService svc = new LopService();
            svc.InsertLop(dt);
            Response.Redirect("Lop.aspx");
        }

        protected void btHuy1_Click(object sender, EventArgs e)
        {
            Response.Redirect("Lop.aspx");
        }
    }
}