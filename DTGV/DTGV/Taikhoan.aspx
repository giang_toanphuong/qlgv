﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Taikhoan.aspx.cs" Inherits="DTGV.Taikhoan1" %>
<%@ Register Assembly="BasicFrame.WebControls.BasicDatePicker" Namespace="BasicFrame.WebControls" TagPrefix="BDP" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4>Thông tin Tài khoản</h4>
        </div>
        <div class="panel-body panel-body-nopadding">
            <div class="danger">
                <asp:Label ID="lblErr" runat="server"></asp:Label>
            </div>
            <div class="form-horizontal form-striped">
                <br />


                <div class="form-group">
                    <label class="col-sm-2 control-label text-right">Tên đăng nhập</label>

                    <div class="col-sm-4">
                        <asp:TextBox runat="server" ID="txtID" CssClass="form-control"></asp:TextBox>
                    </div>
                    <label class="col-sm-2 control-label text-right">Mật khẩu</label>

                    <div class="col-sm-4">
                        <asp:TextBox runat="server" ID="txtPassword" CssClass="form-control"></asp:TextBox>
                    </div>
                </div>
                <div class="form-group">

                    <label class="col-sm-2 control-label text-right">Họ và tên</label>
                    <div class="col-sm-4">
                        <asp:TextBox runat="server" ID="txtHoten" CssClass="form-control"></asp:TextBox>
                    </div>
                    <label class="col-sm-2 control-label text-right">Đơn vị công tác</label>
                    <div class="col-sm-4">
                        <asp:TextBox runat="server" ID="txtDonvicongtac" CssClass="form-control"></asp:TextBox>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-2 control-label text-right">Quyền truy cập</label>
                    <div class="col-sm-4" >
                        <asp:DropDownList ID="ddlQuyen" runat="server" CssClass="form-control">
                            <asp:ListItem Text="chọn quyền" Value="0"></asp:ListItem>
                            <asp:ListItem Text="admin" Value="1"></asp:ListItem>
                            <asp:ListItem Text ="Sở" Value="2"></asp:ListItem>
                            <asp:ListItem Text ="CSDT" Value ="3"></asp:ListItem>
                        </asp:DropDownList>

                    </div>
                    <label class="col-sm-2 control-label text-right">Ghi chú</label>
                    <div class="col-sm-4">
                        <asp:TextBox runat="server" ID="txtGhichu" CssClass="form-control"></asp:TextBox>
                    </div>

                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label text-right">Hình ảnh</label>
                    <br />
                    <div class="col-sm-3">
                        <asp:FileUpload ID="FileUpload2" runat="server" />

                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-9 text-right">
                        <asp:Button ID="txtThemmoi" runat="server" CssClass="btn btn-primary" Text="Thêm Mới" OnClick="txtThemmoi_Click"   />
                    </div>
                                       
                    <div class="col-md-2 text-right">
                        <asp:Button ID="btnTimKiem" runat="server" CssClass="btn btn-primary" Text="Tìm kiếm" OnClick="btnTimKiem_Click"   />
                    </div>
                    
                    

                </div>
                <br />
            </div>
        </div>
    </div>

    <div class="col-sm-8 text-left">
        <h3>Danh sách tài khoản</h3>
    </div>

     <div class="table-responsive">

        <asp:GridView ID="drgTaikhoan" AllowPaging="true" PageSize="10" runat="server" CssClass="table table-striped" AutoGenerateColumns="False" OnPageIndexChanging="drgTaikhoan_PageIndexChanging"  >
            <Columns>
                <asp:TemplateField HeaderText="Tên đăng nhập">
                    <ItemTemplate><%# Eval("Username") %></ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Mật khẩu">
                    <ItemTemplate><%# Eval("Password") %></ItemTemplate>
                </asp:TemplateField>
               
                <asp:TemplateField HeaderText="Họ và tên">
                    <ItemTemplate><%# Eval("Hoten") %></ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Đơn vị công tác">
                    <ItemTemplate><%# Eval("Donvicongtac") %></ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Quyền">
                    <ItemTemplate>
                        <asp:Label ID="lblquyen" runat="server" Text='<%# Eval("Quyen") %>'></asp:Label></ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Ghi chú">
                    <ItemTemplate><%# Eval("Ghichu") %></ItemTemplate>
                </asp:TemplateField>
                

                <asp:TemplateField HeaderText="Cập nhật tài khoản">
                    <ItemTemplate>
                      <asp:LinkButton runat="server" Text="Cập nhật" ID="btnUpdate"  CommandArgument='<%# Eval("Username")%> ' OnCommand="btnUpdate_Command"></asp:LinkButton>
                    </ItemTemplate>
                </asp:TemplateField>
                <%--<asp:TemplateField HeaderText="Chọn">
                    <ItemTemplate>
                        <asp:CheckBox ID="chkChon" runat="server" />
                    </ItemTemplate>
                </asp:TemplateField>--%>
                <%--<asp:TemplateField HeaderText="Xoá tài khoản">
                    <ItemTemplate>
                      <asp:Button runat="server" Text="Xoá" ID="btnDelete" CssClass ="form-control"  />
                    </ItemTemplate>
                </asp:TemplateField>--%>
            </Columns>
        </asp:GridView>


    </div>
</asp:Content>
