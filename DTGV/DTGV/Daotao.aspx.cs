﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DTGV.Services;

namespace DTGV
{
    public partial class Daotao1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["LoginType"].ToString() == "1")
            {
                Response.Redirect("Notpermission.aspx");
            }
            var listDaotao = new DaotaoService().DSDaotao();
            drgDaotao.DataSource = listDaotao;
            drgDaotao.DataBind();
        }
         protected void btnSave_Click(object sender, EventArgs e)
        {
           

        }


        protected void btnView_Command(object sender, CommandEventArgs e)
        {
            var cmnd = e.CommandArgument.ToString();

            Response.Redirect("DaotaoView.aspx?cmnd=" + cmnd);

        }
        
        protected void drgDaotao_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            drgDaotao.PageIndex = e.NewPageIndex;
            drgDaotao.DataBind();
        }
        

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            var listDaotao = new DaotaoService().TimKiem(txtHotenTK.Text, txtCMNDTK.Text, txtGPLXTK.Text, txtTrinhdoTK.Text);

            drgDaotao.DataSource = listDaotao;
            drgDaotao.DataBind();
        }
        

        protected void btnDaotaoAdd_Click(object sender, EventArgs e)
        {
            Response.Redirect("DaotaoAdd.aspx");
        }
    }
}