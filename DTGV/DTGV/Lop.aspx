﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Lop.aspx.cs" Inherits="DTGV.Lop1" %>
<%@ Register Assembly="BasicFrame.WebControls.BasicDatePicker" Namespace="BasicFrame.WebControls" TagPrefix="BDP" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4>Thông tin tìm kiếm</h4>
        </div>
        <div class="panel-body panel-body-nopadding">
            <div class="danger">
                <asp:Label ID="lblErr" runat="server"></asp:Label>
            </div>
            <div class="form-horizontal form-striped">
                <br />
                <div class="form-group">
                    <label class="col-sm-2 control-label text-right">Tên lớp</label>

                    <div class="col-sm-4">
                        <asp:TextBox runat="server" ID="txtTenlop" CssClass="form-control"></asp:TextBox>
                    </div>
                    <label class="col-sm-2 control-label text-right">Mã lớp</label>

                    <div class="col-sm-4">
                        <asp:TextBox runat="server" ID="txtMalop" CssClass="form-control"></asp:TextBox>
                    </div>
                </div>
                <div class="form-group">

                    <label class="col-sm-2 control-label text-right">Trạng thái</label>
                    <div class="col-sm-4">
                        <asp:TextBox runat="server" ID="txtTrangthai" CssClass="form-control"></asp:TextBox>
                    </div>
                    <label class="col-sm-2 control-label text-right">Địa điểm</label>
                    <div class="col-sm-4">
                        <asp:TextBox runat="server" ID="txtDiadiem" CssClass="form-control"></asp:TextBox>
                    </div>
                </div>

                
                <div class="form-group">
                    <div class="col-md-10 text-right">
                        <asp:Button ID="btnTimKiem" runat="server" CssClass="btn btn-primary" Text="Tìm kiếm" OnClick="btnTimKiem_Click" />
                    </div>

                </div>
                <br />
            </div>
        </div>
    </div>


    <div class="col-sm-8 text-left">
        <h3>Danh sách Lớp</h3>
    </div>

    <div class="table-responsive">

        <asp:GridView ID="drgLop" AllowPaging="true" PageSize="2" runat="server" CssClass="table table-striped" AutoGenerateColumns="False" OnPageIndexChanging="drgLop_PageIndexChanging">
            <Columns>
                <asp:TemplateField HeaderText="Tên lớp">
                    <ItemTemplate><%# Eval("Tenlop") %></ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Mã lớp">
                    <ItemTemplate><%# Eval("Malop") %></ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField HeaderText="Ngày lập">
                    <ItemTemplate><%# Eval("Ngaylap") %></ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Ngày bắt đầu">
                    <ItemTemplate><%# Eval("Ngaybatdau") %></ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Ngày kết thúc">
                    <ItemTemplate><%# Eval("Ngayketthuc") %></ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Địa điểm">
                    <ItemTemplate><%# Eval("Diadiem") %></ItemTemplate>
                </asp:TemplateField>


                <asp:TemplateField HeaderText="Thêm mới DSGV">
                    <ItemTemplate>
                        <asp:LinkButton runat="server" Text="Tạo DSGV" ID="btnAddDaotao" CommandArgument='<%# Eval("malop")%> ' OnCommand="btnAddDaotao_Command"></asp:LinkButton>
                    </ItemTemplate>
                </asp:TemplateField>
                <%--<asp:TemplateField HeaderText="Chọn">
                    <ItemTemplate>
                        <asp:CheckBox ID="chkChon" runat="server" />
                    </ItemTemplate>
                </asp:TemplateField>--%>
                <asp:TemplateField HeaderText="Hiển thị DSGV">
                    <ItemTemplate>
                        <asp:LinkButton runat="server" Text="Hiển thị DS" ID="btnLopView" CommandArgument='<%# Eval("malop")%> ' OnCommand="btnLopView_Command"></asp:LinkButton>
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField HeaderText="Cập nhật">
                    <ItemTemplate>
                        <asp:LinkButton runat="server" Text="Cập nhật" ID="btnUpdateLop" CommandArgument='<%# Eval("malop")%> ' OnCommand="btnUpdateLop_Command"></asp:LinkButton>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
        </asp:GridView>
    </div>
    <div class="form-group">
        <div class="col-md-10 text-right">
            <asp:Button ID="btnAddLop" runat="server" CssClass="btn btn-primary" Text="Thêm Lớp" OnClick="btnAddLop_Click" />
        </div>
        <div class="col-md-1 text-center">
            <asp:Button ID="btnInDSLop" runat="server" CssClass="btn btn-primary" Text="In DS lớp" />
        </div>

    </div>
</asp:Content>
