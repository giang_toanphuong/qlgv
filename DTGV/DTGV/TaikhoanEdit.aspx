﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="TaikhoanEdit.aspx.cs" Inherits="DTGV.TaikhoanEdit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3>Thông tin Tài khoản</h3>
        </div>

        <div class="panel-body panel-body-nopadding">

            <div class="form-horizontal form-striped">
                <br />
                <div class="form-group">
                    <label class="col-sm-2 control-label text-right">Tên đăng nhập</label>
                    <div class="col-sm-4">
                        <asp:TextBox ID="txtID" CssClass="form-control" runat="server"></asp:TextBox>
                    </div>
                    <label class="col-sm-2 control-label text-right">Mật khẩu</label>
                    <div class="col-sm-4">
                        <%--<asp:TextBox runat="server" ID="txtNgaysinh" CssClass="form-control"></asp:TextBox>--%>
                        <asp:TextBox ID="txtPassword" CssClass="form-control" runat="server"></asp:TextBox>
                    </div>

                </div>

                <div class="form-group">
                    <label class="col-sm-2 control-label text-right">Họ và tên</label>
                    <div class="col-sm-4">
                        <asp:TextBox runat="server" ID="txtHoten" CssClass="form-control"></asp:TextBox>
                    </div>

                    <label class="col-sm-2 control-label text-right">Đơn vị công tác</label>
                    <div class="col-sm-4">
                        <asp:TextBox runat="server" ID="txtDonvi" CssClass="form-control"></asp:TextBox>
                    </div>

                </div>

                
                <div class="form-group">
                    <label class="col-sm-2 control-label text-right">Quyền truy cập</label>
                    <div class="col-sm-4">
                        <asp:DropDownList ID="ddlQuyen" runat="server">
                            <asp:ListItem Text ="Admin" Value="1"></asp:ListItem>
                            <asp:ListItem Text ="Sở" Value ="2"></asp:ListItem>
                            <asp:ListItem Text ="CSDT" Value ="3"></asp:ListItem>
                            <asp:ListItem Text="Lựa chọn quyền" Value="0"></asp:ListItem>
                        </asp:DropDownList>
                        
                    </div>
                    <label class="col-sm-2 control-label text-right">Ghi chú</label>
                    <div class="col-sm-4">
                        <asp:TextBox runat="server" ID="txtGhichu" CssClass="form-control"></asp:TextBox>
                       
                    </div>

                </div>
                
                <div class="form-group">
                    <div class="col-md-10 text-right">
                        <asp:Button ID="btnLuu" runat="server" CssClass="btn btn-primary" Text="Luu DL" OnClick="btnLuu_Click" />
                    </div>
                    <div class="col-md-1 text-right">
                        <asp:Button ID="btHuy1" runat="server" CssClass="btn btn-primary" Text="Huỷ" OnClick="btHuy1_Click"/>
                    </div>

                </div>
                <br />
            </div>
        </div>
    </div>
</asp:Content>
