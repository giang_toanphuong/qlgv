﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DTGV.Services;

namespace DTGV
{
    public partial class TaikhoanEdit : System.Web.UI.Page
    {
        UtilityService usv = new UtilityService();
        protected void Page_Load(object sender, EventArgs e)
        {
            var _username = Request.QueryString["username"].ToString();
            txtID.Text = _username;
        }

        protected void btnLuu_Click(object sender, EventArgs e)
        {
            if(!IsPostBack)
            {
                var _username = Request.QueryString["username"].ToString();
                txtID.Text = _username;
                Taikhoan dt = new Taikhoan();
                dt.Username = _username;
                dt.Password = usv.Md5Gen(txtPassword.Text);
                dt.Hoten = txtHoten.Text;

                dt.Donvicongtac = txtDonvi.Text;
                dt.Ghichu = txtGhichu.Text;
                dt.Quyen = int.Parse(ddlQuyen.SelectedValue.ToString());

                TaikhoanService svc = new TaikhoanService();
                svc.UpdateTaikhoan(dt);
            }
           
        }

        protected void btHuy1_Click(object sender, EventArgs e)
        {
            Response.Redirect("Taikhoan.aspx");
        }
    }
}