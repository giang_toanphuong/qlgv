﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="QuyetdinhDatAdd.aspx.cs" Inherits="DTGV.QuyetdinhDatAdd" %>
<%@ Register Assembly="BasicFrame.WebControls.BasicDatePicker" Namespace="BasicFrame.WebControls" TagPrefix="BDP" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3>Thông tin Quyết định Đạt</h3>
        </div>

        <div class="panel-body panel-body-nopadding">

            <div class="form-horizontal form-striped">
                <br />
                <div class="form-group">
                    <label class="col-sm-2 control-label text-right">Số quyết định</label>
                    <div class="col-sm-4">
                        <asp:TextBox ID="txtsoQD" CssClass="form-control" runat="server"></asp:TextBox>
                    </div>
                    <label class="col-sm-2 control-label text-right">Mã lớp</label>
                    <div class="col-sm-4">
                        <%--<asp:TextBox runat="server" ID="txtNgaysinh" CssClass="form-control"></asp:TextBox>--%>
                        <asp:TextBox ID="txtMalop" CssClass="form-control" runat="server"></asp:TextBox>
                    </div>

                </div>

                <div class="form-group">
                    

                    <label class="col-sm-2 control-label text-right">Nội dung</label>
                    <div class="col-sm-4">
                        <asp:TextBox runat="server" ID="txtNoidung" CssClass="form-control"></asp:TextBox>
                    </div>
                    <label class="col-sm-2 control-label text-right">Ngày lập</label>
                    <div class="col-sm-4">
                        <BDP:BDPLite ID="bdpNgaylap" CssClass="form-control" DateFormat="d" runat="server">
                        </BDP:BDPLite>
                    </div>
                </div>

                
                <div class="form-group">
                    
                    <label class="col-sm-2 control-label text-right">Trạng thái</label>
                    <div class="col-sm-4">
                        <asp:TextBox runat="server" ID="txtTrangthai" CssClass="form-control"></asp:TextBox>
                       
                    </div>
                     <label class="col-sm-2 control-label text-right">Ghi chú</label>
                    <div class="col-sm-4">
                        <asp:TextBox runat="server" ID="txtGhichu" CssClass="form-control"></asp:TextBox>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-10 text-right">
                        <asp:Button ID="btnLuu" runat="server" CssClass="btn btn-primary" Text="Luu DL" OnClick="btnLuu_Click"  />
                    </div>
                    <div class="col-md-1 text-right">
                        <asp:Button ID="btHuy1" runat="server" CssClass="btn btn-primary" Text="Huỷ" OnClick="btHuy1_Click" />
                    </div>

                </div>
                <br />
            </div>
        </div>
    </div>
</asp:Content>
