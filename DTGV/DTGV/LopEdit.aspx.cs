﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DTGV.Services;

namespace DTGV
{
    public partial class LopEdit : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            var _malop = Request.QueryString["malop"].ToString();
            txtMalop.Text = _malop;
        }

        protected void btnLuu_Click(object sender, EventArgs e)
        {
            
            
                var _malop = Request.QueryString["malop"].ToString();
                Lop dt = new Lop();
                dt.Tenlop = txtTenlop.Text;
            dt.Malop = _malop;
                dt.Soluong = int.Parse(txtSoluong.Text);
                dt.Ngaylap = bdpNgaylap.SelectedDate;
                dt.Ngaybatdau = bdpNgaybatdau.SelectedDate;
                dt.Ngayketthuc = bdpNgayketthuc.SelectedDate;
                dt.Diadiem = txtDiadiem.Text;
                dt.Ghichu = txtGhichu.Text;
                if (DateTime.Compare(bdpNgaybatdau.SelectedDate, DateTime.Now) == -1)
                {
                    dt.Trangthai = "Chưa bắt đầu";
                }
                else
                {
                    if (DateTime.Compare(bdpNgayketthuc.SelectedDate, DateTime.Now) == 1)
                    {
                        dt.Trangthai = "Đang diễn ra";
                    }
                    else
                    {
                        dt.Trangthai = "Đã kết thúc";
                    }
                }
                LopService svc = new LopService();
                svc.UpdateLop(dt);
                Response.Redirect("Lop.aspx");
            
        }

        protected void btHuy1_Click(object sender, EventArgs e)
        {
            Response.Redirect("Lop.aspx");
        }
    }
}