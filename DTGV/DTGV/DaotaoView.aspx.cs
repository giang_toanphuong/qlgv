﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DTGV.Services;

namespace DTGV
{
    public partial class Daotao_View : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                var _cmnd = Request.QueryString["cmnd"].ToString();
                var ListHoso = new DaotaoService().GetDaotaoID(_cmnd);

                txtHoten.Text = ListHoso[0].Hoten;
                txtNgaysinh.Text = ListHoso[0].Ngaysinh.ToString();
                txtCMND.Text = ListHoso[0].Cmnd;
                txtGioitinh.Text = ListHoso[0].Gioitinh;
                txtGPLX.Text = ListHoso[0].GPLX;
                //txtHangGPLX.Text = ListHoso[0].HangGPLX;
                txtHangDT.Text = ListHoso[0].Hangdaotao;
                //txtNgaycap.Text = ListHoso[0].NgaycapGPLX.ToString();
                //txtNgayhethan.Text = ListHoso[0].NgayhethanGPLX.ToString();
                txtvanhoa.Text = ListHoso[0].Vanhoa;
                txtQue.Text = ListHoso[0].Que;
                txtCCSP.Text = ListHoso[0].CCsupham;
                txtTenCSDT.Text = ListHoso[0].TenCSDT;
                txtTenSo.Text = ListHoso[0].TenSo;
                txtMaLop.Text = ListHoso[0].MaLop;
                txtGhichu.Text = ListHoso[0].Ghichu;
                var fileName = ListHoso[0].Hinhanh;
                if(!string.IsNullOrEmpty(fileName))
                {
                    Image1.ImageUrl = "AnhHoso/" + fileName;
                }
                

            }
        }

        protected void btHuy1_Click(object sender, EventArgs e)
        {
            Response.Redirect("Daotao.aspx");
        }
    }
}