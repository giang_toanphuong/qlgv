﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="LopEdit.aspx.cs" Inherits="DTGV.LopEdit" %>
<%@ Register Assembly="BasicFrame.WebControls.BasicDatePicker" Namespace="BasicFrame.WebControls" TagPrefix="BDP" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3>Thông tin Lớp</h3>
        </div>

        <div class="panel-body panel-body-nopadding">

            <div class="form-horizontal form-striped">
                <br />
                <div class="form-group">
                    <label class="col-sm-2 control-label text-right">Tên lớp</label>
                    <div class="col-sm-4">
                        <asp:TextBox ID="txtTenlop" CssClass="form-control" runat="server"></asp:TextBox>
                    </div>
                    <label class="col-sm-2 control-label text-right">Mã lớp</label>
                    <div class="col-sm-4">
                        <asp:TextBox ID="txtMalop" CssClass="form-control" runat="server"></asp:TextBox>
                    </div>

                </div>

                <div class="form-group">
                    <label class="col-sm-2 control-label text-right">Số lượng</label>
                    <div class="col-sm-4">
                        <asp:TextBox ID="txtSoluong" CssClass="form-control" runat="server"></asp:TextBox>
                    </div>

                    <label class="col-sm-2 control-label text-right">Địa điểm</label>
                    <div class="col-sm-4">
                        <asp:TextBox runat="server" ID="txtDiadiem" CssClass="form-control"></asp:TextBox>
                    </div>

                </div>


                <div class="form-group">
                    <label class="col-sm-2 control-label text-right">Ngày lập</label>
                    <div class="col-sm-4">
                        <BDP:BDPLite ID="bdpNgaylap" CssClass="form-control" DateFormat="d" runat="server">
                        </BDP:BDPLite>
                    </div>
                    <label class="col-sm-2 control-label text-right">Ngày bắt đầu</label>
                    <div class="col-sm-4">
                        <BDP:BDPLite ID="bdpNgaybatdau" CssClass="form-control" DateFormat="d" runat="server">
                        </BDP:BDPLite>
                    </div>
                    

                </div>
                <div class="form-group">
                    
                    <label class="col-sm-2 control-label text-right">Ngày kết thúc</label>
                    <div class="col-sm-4">
                        <BDP:BDPLite ID="bdpNgayketthuc" CssClass="form-control" DateFormat="d" runat="server">
                        </BDP:BDPLite>
                    </div>
                    <label class="col-sm-2 control-label text-right">Ghi chú</label>
                    <div class="col-sm-4">
                        <asp:TextBox runat="server" ID="txtGhichu" CssClass="form-control"></asp:TextBox>
                    </div>
                </div>



               <%-- <div class="form-group">
                    
                </div>--%>

                <div class="form-group">
                    <div class="col-md-10 text-right">
                        <asp:Button ID="btnLuu" runat="server" CssClass="btn btn-primary" Text="Luu DL" OnClick="btnLuu_Click" />
                    </div>
                    <div class="col-md-1 text-right">
                        <asp:Button ID="btHuy1" runat="server" CssClass="btn btn-primary" Text="Huỷ" OnClick="btHuy1_Click" />
                    </div>

                </div>
                <br />
            </div>
        </div>
    </div>
</asp:Content>
