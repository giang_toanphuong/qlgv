﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DTGV.Services;

namespace DTGV
{
    public partial class Quyetdinh : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {

                if (Session["LoginType"].ToString() != "2")
                {
                    Response.Redirect("Notpermission.aspx");
                }
                string _so = Session["Donvi"].ToString();
                var listLop = new LopService().DanhsachLop(Session["Donvi"].ToString());
                ddlMalop.DataSource = listLop;
                ddlMalop.DataTextField = "Tenlop";
                ddlMalop.DataValueField = "Malop";
                ddlMalop.DataBind();
                //int quyen = int.Parse(Session["LoginType"].ToString());
                var listQD = new QuyetdinhKtraService().DanhSachQuyetdinh();
                drgQD.DataSource = listQD;
                drgQD.DataBind();
                //var listLop = new LopService().DanhsachLop(_so);
                //drgLop.DataSource = listLop;
                //drgLop.DataBind();

            }
        }
        private bool validData()
        {
            if (string.IsNullOrEmpty(txtSoQuyetDinh.Text))
            {
                lblErr.Text = "Nhập lại số quyết định";
                return false;
            }
            if (string.IsNullOrEmpty(txtTrangthai.Text))
            {
                lblErr.Text = "Nhập lại trạng thái";
                return false;
            }
            if (string.IsNullOrEmpty(txtDiadiem.Text))
            {
                lblErr.Text = "Nhập lại địa điểm";
                return false;
            }
            //if (string.IsNullOrEmpty(txtMalop.Text))
            //{
            //    lblErr.Text = "Nhập mã lớp";
            //    return false;
            //}

            return true;
        }
        protected void drgQD_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            drgQD.PageIndex = e.NewPageIndex;
            drgQD.DataBind();
        }
        protected void btnTimKiem_Click(object sender, EventArgs e)
        {
            if ((string.IsNullOrEmpty(txtSoQuyetDinh.Text)) && (ddlMalop.SelectedValue == "0"))
            {
                var listQD = new QuyetdinhKtraService().DanhSachQuyetdinh();
                drgQD.DataSource = listQD;
                drgQD.DataBind();
            }
            else
            {
                var listQD = new QuyetdinhKtraService().TimKiem(int.Parse(txtSoQuyetDinh.Text), txtTrangthai.Text, ddlMalop.SelectedValue, txtDiadiem.Text);
                drgQD.DataSource = listQD;
                drgQD.DataBind();
            }
        }
        protected void btnEdit_Command(object sender, CommandEventArgs e)
        {
            var _soquyetdinh = e.CommandArgument.ToString();

            Response.Redirect("QuyetdinhKtraEdit.aspx?soquyetdinh=" + _soquyetdinh);

        }
        protected void btnThemQD_Click(object sender, EventArgs e)
        {
            Response.Redirect("QuyetdinhKtraAdd.aspx");
        }
    }
}