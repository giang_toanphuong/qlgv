﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Quyetdinhdat.aspx.cs" Inherits="DTGV.Quyetdinhdat" %>

<%@ Register Assembly="BasicFrame.WebControls.BasicDatePicker" Namespace="BasicFrame.WebControls" TagPrefix="BDP" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4>Thông tin tìm kiếm</h4>
        </div>
        <div class="panel-body panel-body-nopadding">
            <div class="danger">
                <asp:Label ID="lblErr" runat="server"></asp:Label>
            </div>
            <div class="form-horizontal form-striped">
                <br />
                <div class="form-group">
                    <label class="col-sm-2 control-label text-right">Số Quyết định</label>
                    <div class="col-sm-4">
                        <asp:TextBox runat="server" ID="txtSoQuyetDinh" CssClass="form-control"></asp:TextBox>
                    </div>

                    <label class="col-sm-2 control-label text-right">Mã lớp</label>
                    <div class="col-sm-4">
                        <%--<asp:TextBox runat="server" ID="txtMalop" CssClass="form-control"></asp:TextBox>--%>
                        <asp:DropDownList ID="ddlMalop" runat="server" CssClass ="form-control"></asp:DropDownList>
                    </div>
                </div>               
                
               
                
                <div class="form-group">
                    <div class="col-md-10 text-right">
                        <asp:Button ID="btnTimKiem" runat="server" CssClass="btn btn-primary" Text="Tìm kiếm" OnClick="btnTimKiem_Click"  />
                    </div>

                </div>
                <br />
            </div>
        </div>
    </div>


    <div class="col-sm-8 text-left">
        <h3>Danh sách Quyết định</h3>
    </div>

    <div class="table-responsive">

        <asp:GridView ID="drgQDDat" AllowPaging="true" PageSize="2" runat="server" CssClass="table table-striped" AutoGenerateColumns="False" OnPageIndexChanging="drgQDDat_PageIndexChanging">
            <Columns>
                <asp:TemplateField HeaderText="Số quyết định">
                    <ItemTemplate><%# Eval("Soquyetdinh") %></ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Mã lớp">
                    <ItemTemplate><%# Eval("Malop") %></ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Nội dung">
                    <ItemTemplate><%# Eval("Noidung") %></ItemTemplate>
                </asp:TemplateField>
                
                <asp:TemplateField HeaderText="Ngày lập">
                    <ItemTemplate><%# Eval("Ngaylap") %></ItemTemplate>
                </asp:TemplateField>
               
                
               
                <%--<asp:TemplateField HeaderText="Chọn">
                    <ItemTemplate>
                        <asp:CheckBox ID="chkChon" runat="server" />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:CommandField EditText="Sửa" CancelText="Huỷ bỏ" UpdateText="Cập nhật" ShowEditButton="True" AccessibleHeaderText="Sửa" />
                <asp:CommandField DeleteText="Xoá" ShowDeleteButton="True" AccessibleHeaderText="Xoá" />--%>

                <%--<asp:TemplateField HeaderText="Thêm DS">
                    <ItemTemplate>
                        <asp:LinkButton runat="server" Text="Thêm DS" ID="btnAdd"  OnCommand="btnAdd_Command"></asp:LinkButton>
                    </ItemTemplate>
                </asp:TemplateField>--%>
                <asp:TemplateField HeaderText="Thêm DSGV">
                    <ItemTemplate>
                        <asp:LinkButton runat="server" Text="Tạo DSGV" ID="btnAddDaotao" CommandArgument='<%# Eval("malop")%> ' OnCommand="btnAddDaotao_Command"></asp:LinkButton>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Cập nhật">
                    <ItemTemplate>
                        <asp:LinkButton runat="server" Text="Cập nhật" ID="btnedit" CommandArgument='<%# Eval("soquyetdinh")%> ' OnCommand="btnEdit_Command"></asp:LinkButton>
                        <%--<asp:LinkButton runat="server" Text="Cập nhật" ID="LinkButton1" CommandArgument='<%# Eval("soquyetdinh")+ "," + Eval("ngaylap")%> ' OnCommand="btnEdit_Command"></asp:LinkButton>--%>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
        </asp:GridView>


    </div>
    <div class="form-group">
        <div class="col-md-10 text-right">
            <asp:Button ID="btnThemQD" runat="server" CssClass="btn btn-primary" Text="Thêm QD" OnClick="btnThemQD_Click" />
        </div>
        <div class="col-md-1 text-center">
            <asp:Button ID="btnInDSLop" runat="server" CssClass="btn btn-primary" Text="In DS Đào tạo" />
        </div>

    </div>
</asp:Content>
