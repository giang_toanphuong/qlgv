﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;

namespace DTGV.Services
{
    public class LopService
    {
        public List<Lop> DanhsachLop(string donvi )
        {
           
                using (var db = new DTGVDataContext())
                {
                Lop _Lop = new Lop();
                List<Lop> listlop = new List<Lop>();
                try
                {
                    So _maso = db.Sos.FirstOrDefault(p => p.TenDV == donvi);
                     _Lop = db.Lops.FirstOrDefault(p => p.MaSo  == _maso.MaSo);
                     listlop = db.Lops.Where(p => p.Malop == _Lop.Malop).ToList();

                }
                catch(Exception ex)
                {
                    
                }
                return listlop;
            }
            
            
            
        }

        public List<Lop> TimKiem(string tenlop, string malop, string diadiem, string trangthai)
        {

            using (var db = new DTGVDataContext())
            {
                var query = from p in db.Lops select p;
                if (!string.IsNullOrEmpty(tenlop))
                    query = query.Where(p => p.Tenlop == tenlop);
                if (!string.IsNullOrEmpty(malop))
                    query = query.Where(p => p.Malop == malop);
                if (!string.IsNullOrEmpty(diadiem))
                    query = query.Where(p => p.Diadiem == diadiem);
                if (!string.IsNullOrEmpty(trangthai))
                    query = query.Where(p => p.Trangthai == trangthai);
                return query.ToList();

            }
        }
        public List<Lop> GetLopID(string _malop)
        {
            using (var db = new DTGVDataContext())
            {
                return db.Lops.Where(p => p.Malop == _malop || _malop == null).ToList();
            }
        }
        public void UpdateLop(Lop DT)
        {
            using (var db = new DTGVDataContext())
            {
                var item = db.Lops.FirstOrDefault(p => p.Malop == DT.Malop);
                if (item != null)
                {
                    item.Tenlop = DT.Tenlop;
                    item.Soluong = DT.Soluong;
                    
                    item.Ngaylap = DT.Ngaylap;
                    item.Ngaybatdau = DT.Ngaybatdau;
                    item.Ngayketthuc = DT.Ngayketthuc;
                    item.Diadiem = DT.Diadiem;
                    item.Trangthai = DT.Trangthai;
                    
                    item.Ghichu = DT.Ghichu;
                    
                    db.SubmitChanges();

                }
            }
        }
        public void InsertLop(Lop dt)
        {
            using (var db = new DTGVDataContext())
            {
                db.Lops.InsertOnSubmit(dt);
                db.SubmitChanges();

            }
        }
        public void DeleteDaotao(Lop dt)
        {
            using (var db = new DTGVDataContext())
            {
                var item = db.Lops.FirstOrDefault(p => p.Malop == dt.Malop);
                if (item != null)
                {
                    db.Lops.DeleteOnSubmit(item);
                    db.SubmitChanges();
                }


            }
        }

    }
}