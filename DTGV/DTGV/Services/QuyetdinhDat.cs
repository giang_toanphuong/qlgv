﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DTGV.Services
{
    public class QuyetdinhDatService
    {
        public List<QuyetdinhDat> DanhSachQuyetdinh()
        {
            using (var db = new DTGVDataContext())
            {
                return db.QuyetdinhDats.ToList();

            }
        }

        public List<QuyetdinhDat> TimKiem(int _soquyetdinh, string _malop)
        {

            using (var db = new DTGVDataContext())
            {
                var query = from p in db.QuyetdinhDats select p;
                if (!string.IsNullOrEmpty(_malop))
                    query = query.Where(p => p.Malop == _malop);
                if (_soquyetdinh > 0)
                    query = query.Where(p => p.Soquyetdinh == _soquyetdinh);
                return query.ToList();
            }
        }
        public List<QuyetdinhDat> GetQDID(int quyetdinhID)
        {
            using (var db = new DTGVDataContext())
            {
                return db.QuyetdinhDats.Where(p => p.Soquyetdinh == quyetdinhID || quyetdinhID == 0).ToList();
            }
        }
        public void UpdateQuyetdinh(QuyetdinhDat DT)
        {
            using (var db = new DTGVDataContext())
            {
                var item = db.QuyetdinhDats.FirstOrDefault(p => p.Soquyetdinh == DT.Soquyetdinh);
                if (item != null)
                {
                    item.Ghichu = DT.Ghichu;
                    item.Malop = DT.Malop;
                    item.Noidung = DT.Noidung;

                    item.Trangthai = DT.Trangthai;
                    db.SubmitChanges();

                }
            }
        }
        public void InsertQuyetdinh(QuyetdinhDat dt)
        {
            using (var db = new DTGVDataContext())
            {
                try
                {
                    db.QuyetdinhDats.InsertOnSubmit(dt);
                    db.SubmitChanges();
                }
                catch(Exception ex)
                { }
                

            }
        }
        public void DeleteQuyetdinh(QuyetdinhDat dt)
        {
            using (var db = new DTGVDataContext())
            {
                var item = db.QuyetdinhDats.FirstOrDefault(p => p.Soquyetdinh == dt.Soquyetdinh);
                if (item != null)
                {
                    db.QuyetdinhDats.DeleteOnSubmit(item);
                    db.SubmitChanges();
                }


            }
        }
    }
}