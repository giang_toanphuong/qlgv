﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;

namespace DTGV.Services
{
    public class QuyetdinhKtraService
    {
        public List<QuyetdinhKTra> DanhSachQuyetdinh()
        {
            using (var db = new DTGVDataContext())
            {
                return db.QuyetdinhKTras.ToList();

            }
        }

        public List<QuyetdinhKTra> TimKiem(int _soQD, string _malop, string trangthai, string diadiem)
        {

            using (var db = new DTGVDataContext())
            {
                var query = from p in db.QuyetdinhKTras select p;
                if (_soQD>0)
                    query = query.Where(p => p.Soquyetdinh == _soQD);
                if (!string.IsNullOrEmpty(_malop))
                    query = query.Where(p => p.Malop == _malop);                
                if (!string.IsNullOrEmpty(diadiem))
                    query = query.Where(p => p.Diadiem == diadiem);
                if (!string.IsNullOrEmpty(trangthai))
                    query = query.Where(p => p.Trangthai == trangthai);
                return query.ToList();

            }
        }
        public List<QuyetdinhKTra> GetQDID(int quyetdinhID)
        {
            using (var db = new DTGVDataContext())
            {
                return db.QuyetdinhKTras.Where(p => p.Soquyetdinh == quyetdinhID || quyetdinhID == 0).ToList();
            }
        }
        public void UpdateQuyetdinh(QuyetdinhKTra DT)
        {
            using (var db = new DTGVDataContext())
            {
                var item = db.QuyetdinhKTras.FirstOrDefault(p => p.Soquyetdinh == DT.Soquyetdinh);
                if (item != null)
                {
                    item.Ghichu = DT.Ghichu;
                    item.Malop = DT.Malop;
                    item.Noidung = DT.Noidung;
                    
                    item.Ngaybatdau = DT.Ngaybatdau;
                    item.Ngayketthuc = DT.Ngayketthuc;
                    
                    item.Trangthai = DT.Trangthai;
                    item.Diadiem = DT.Diadiem;

                    
                    db.SubmitChanges();

                }
            }
        }
        public void InsertQuyetdinh(QuyetdinhKTra dt)
        {
            using (var db = new DTGVDataContext())
            {
                db.QuyetdinhKTras.InsertOnSubmit(dt);
                db.SubmitChanges();

            }
        }
        public void DeleteQuyetdinh(QuyetdinhKTra dt)
        {
            using (var db = new DTGVDataContext())
            {
                var item = db.QuyetdinhKTras.FirstOrDefault(p => p.Soquyetdinh == dt.Soquyetdinh);
                if (item != null)
                {
                    db.QuyetdinhKTras.DeleteOnSubmit(item);
                    db.SubmitChanges();
                }


            }
        }
    }
}