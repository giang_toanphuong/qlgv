﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;

namespace DTGV.Services
{
    public class TaikhoanService
    {
        public List<Taikhoan> DanhsachTaikhoan()
        {
            using (var db = new DTGVDataContext())
            {
                return db.Taikhoans.ToList();

            }
        }

        public List<Taikhoan> TimKiem(string tenDangnhap, string matkhau, string hoten, string donvicongtac)
        {

            using (var db = new DTGVDataContext())
            {
                var query = from p in db.Taikhoans select p;
                if (!string.IsNullOrEmpty(tenDangnhap))
                    query = query.Where(p => p.Username == tenDangnhap);
                if (!string.IsNullOrEmpty(matkhau))
                    query = query.Where(p => p.Password == matkhau);
                if (!string.IsNullOrEmpty(hoten))
                    query = query.Where(p => p.Hoten == hoten);
                if (!string.IsNullOrEmpty(donvicongtac))
                    query = query.Where(p => p.Donvicongtac == donvicongtac);
                return query.ToList();

            }
        }
        public List<Taikhoan> GetTaikhoanID(string taikhoanID)
        {
            using (var db = new DTGVDataContext())
            {
                return db.Taikhoans.Where(p => p.Username == taikhoanID || taikhoanID == null).ToList();
            }
        }
        public void UpdateTaikhoan(Taikhoan DT)
        {
            using (var db = new DTGVDataContext())
            {
                var item = db.Taikhoans.FirstOrDefault(p => p.Username == DT.Username);
                if (item != null)
                {
                    item.Ghichu = DT.Ghichu;
                    item.Password = DT.Password;
                    item.Hoten = DT.Hoten;
                    item.Donvicongtac = DT.Donvicongtac;
                    item.Hoten = DT.Hoten;
                    item.Quyen = DT.Quyen;
                    db.SubmitChanges();

                }
            }
        }
        public void InsertTaikhoan(Taikhoan dt)
        {
            using (var db = new DTGVDataContext())
            {
                try
                {
                    db.Taikhoans.InsertOnSubmit(dt);
                    db.SubmitChanges();
                }
                catch (Exception ex)
                {

                }


            }
        }
        public void DeleteDaotao(Taikhoan dt)
        {
            using (var db = new DTGVDataContext())
            {
                var item = db.Taikhoans.FirstOrDefault(p => p.Username == dt.Username);
                if (item != null)
                {
                    db.Taikhoans.DeleteOnSubmit(item);
                    db.SubmitChanges();
                }


            }
        }
    }
}