﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DTGV.Services;

namespace DTGV.Services
{
    public class AcountService
    {
        UtilityService svc = new UtilityService();
        public Taikhoan CheckSignin(string userName, string pass)
        {
            using (var db = new DTGVDataContext())
            {
                //return db.Taikhoans.FirstOrDefault(p => p.Username.ToUpper() == userName.ToUpper() && svc.Md5Gen(pass) == p.Password && p.Quyen ==quyen  && (p.IsEnable ?? false));
                return db.Taikhoans.FirstOrDefault(p => p.Username.ToUpper() == userName.ToUpper() && svc.Md5Gen(pass) == p.Password );
            }
        }
    }
}