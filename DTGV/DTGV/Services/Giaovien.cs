﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DTGV.Services
{
    public class GiaovienService
    {
        public List<Giaovien> DanhSachGiaoVien()
        {
            using (var db = new DTGVDataContext())
            {
                return db.Giaoviens.ToList();

            }
        }

        public List<Giaovien> GetGiaovienID(string GiaovienID)
        {
            using (var db = new DTGVDataContext())
            {

                return db.Giaoviens.Where(p => p.Cmnd == GiaovienID || GiaovienID == null).ToList();
            }
        }
        public List<Giaovien> TimKiem(string hoten, string cmnd, string gplx,string vanhoa)
        {
            using (var db = new DTGVDataContext())
            {
                var query = from p in db.Giaoviens select p;
                if (!string.IsNullOrEmpty(hoten))
                    query = query.Where(p => p.Hoten == hoten);
                if (!string.IsNullOrEmpty(gplx))
                    query = query.Where(p => p.GPLX == gplx);
                if (!string.IsNullOrEmpty(vanhoa))
                    query = query.Where(p => p.Vanhoa == vanhoa);
                if (!string.IsNullOrEmpty(cmnd))
                    query = query.Where(p => p.Cmnd == cmnd);
                return query.ToList();

            }
        }
        public void UpdateGiaovien(Giaovien DT)
        {
            using (var db = new DTGVDataContext())
            {
                var item = db.Giaoviens.FirstOrDefault(p => p.GPLX == DT.GPLX);
                if (item != null)
                {
                    item.Ghichu = DT.Ghichu;
                    item.Gioitinh = DT.Gioitinh;

                    item.Hoten = DT.Hoten;

                    item.TenCSDT = DT.TenCSDT;

                    //item.Ngaycap = DT.Ngaycap;
                    //item.Ngayhethan = DT.Ngayhethan;

                    item.Ngaysinh = DT.Ngaysinh;
                    item.Que = DT.Que;

                    item.Vanhoa = DT.Vanhoa;
                    item.CCSupham = DT.CCSupham;
                    item.Cmnd = DT.Cmnd;
                    item.Vanhoa = DT.Vanhoa;
                    db.SubmitChanges();
                    
                }
            }
        }
        public void InsertGiaovien(Giaovien dt)
        {
            using (var db = new DTGVDataContext())
            {
                db.Giaoviens.InsertOnSubmit(dt);
                db.SubmitChanges();
               
            }
        }
        public void DeleteLop(Giaovien dt)
        {
            using (var db = new DTGVDataContext())
            {
                var item = db.Giaoviens.FirstOrDefault(p => p.GPLX == dt.GPLX);
                if (item != null)
                {
                    db.Giaoviens.DeleteOnSubmit(item);
                }
                db.SubmitChanges();
                
            }
        }
    }
}