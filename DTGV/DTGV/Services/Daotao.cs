﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;

namespace DTGV.Services
{
    public class DaotaoService
    {
        public List<Daotao> DSDaotao()
        {
                using (var db = new DTGVDataContext())
                {
                    return db.Daotaos.ToList();
                }
           
            
        }
        

        //public List<Daotao> DSDaotao(string donvi, int quyen)
        //{
        //    //hien thi theo csdt
        //    if (quyen == 3)
        //    {
        //        using (var db = new DTGVDataContext())
        //        {
        //            //lay ma csdt
        //            var _csdt = db.CSDTs.FirstOrDefault(p => p.TenCSDT == donvi);
        //            string maCSDT = _csdt.MaCSDT;
        //            return db.Daotaos.Where(p => p.MaCSDT == maCSDT).ToList();
        //        }
        //    }
        //    //hien thi theo so
        //    else
        //    {
        //        using (var db = new DTGVDataContext())
        //        {
        //            //lay ma so
        //            var _so = db.Sos.FirstOrDefault(p => p.TenDV == donvi);
        //            List<CSDT> ListCSDT = db.CSDTs.Where(p => p.MaSo == _so.MaSo).ToList();
        //            List<Daotao> ListDaotao = new List<Daotao>();
        //            foreach (var item in ListCSDT)
        //            {
        //                List<Daotao> listTam = db.Daotaos.Where(p => p.MaCSDT == item.MaCSDT).ToList();
        //                foreach (var itemDT in listTam)
        //                {
        //                    ListDaotao.Add(itemDT);
        //                }

        //            }
        //            //danh sach dao tao duoc them moi tai so
        //            List<Daotao> listDT_So = db.Daotaos.Where(p => p.MaSo == _so.MaSo).ToList();
        //            foreach (var itemDT in listDT_So)
        //            {
        //                ListDaotao.Add(itemDT);
        //            }
        //            return ListDaotao;
        //        }


        //    }

        //}

        public List<Daotao> TimKiem(string hoten, string cmnd,string gplx, string vanhoa )
        {
            //tim kiem trong csdt
           
                using (var db = new DTGVDataContext())
                {
                    //lay ma csdt
                    
                    var query = from p in db.Daotaos select p;
                    if (!string.IsNullOrEmpty(hoten))
                        query = query.Where(p => p.Hoten == hoten);
                    if (!string.IsNullOrEmpty(gplx))
                        query = query.Where(p => p.GPLX == gplx);
                    if (!string.IsNullOrEmpty(vanhoa))
                        query = query.Where(p => p.Vanhoa == vanhoa);
                    if (!string.IsNullOrEmpty(cmnd))
                        query = query.Where(p => p.Cmnd == cmnd);
                    return query.ToList();

                }
          
            
        }

        //public List<Daotao> TimKiem(string hoten, string gplx, string vanhoa, string cmnd, string donvi, int quyen)
        //{
        //    //tim kiem trong csdt
        //    if (quyen == 3)
        //    {
        //        using (var db = new DTGVDataContext())
        //        {
        //            //lay ma csdt
        //            var _csdt = db.CSDTs.FirstOrDefault(p => p.TenCSDT == donvi);
        //            string maCSDT = _csdt.MaCSDT;
        //            var query = from p in db.Daotaos.Where(p => p.MaCSDT == maCSDT) select p;
        //            if (!string.IsNullOrEmpty(hoten))
        //                query = query.Where(p => p.Hoten == hoten);
        //            if (!string.IsNullOrEmpty(gplx))
        //                query = query.Where(p => p.GPLX == gplx);
        //            if (!string.IsNullOrEmpty(vanhoa))
        //                query = query.Where(p => p.Vanhoa == vanhoa);
        //            if (!string.IsNullOrEmpty(cmnd))
        //                query = query.Where(p => p.Cmnd == cmnd);
        //            return query.ToList();

        //        }
        //    }
        //    else
        //    {
        //        //chua xet dieu kien cua so
        //        using (var db = new DTGVDataContext())
        //        {
        //            var query = from p in db.Daotaos select p;
        //            if (!string.IsNullOrEmpty(hoten))
        //                query = query.Where(p => p.Hoten == hoten);
        //            if (!string.IsNullOrEmpty(gplx))
        //                query = query.Where(p => p.GPLX == gplx);
        //            if (!string.IsNullOrEmpty(vanhoa))
        //                query = query.Where(p => p.Vanhoa == vanhoa);
        //            if (!string.IsNullOrEmpty(cmnd))
        //                query = query.Where(p => p.Cmnd == cmnd);
        //            return query.ToList();

        //        }
        //    }

        //}
        public List<Daotao> GetDaotaoID(string DaotaoID)
        {
            using (var db = new DTGVDataContext())
            {
                return db.Daotaos.Where(p => p.Cmnd == DaotaoID || DaotaoID == null).ToList();
            }
        }
        public void UpdateDaotao(Daotao DT)
        {
            using (var db = new DTGVDataContext())
            {
                var item = db.Daotaos.FirstOrDefault(p => p.Cmnd == DT.Cmnd);
                if (item != null)
                {
                    item.Ghichu = DT.Ghichu;
                    item.Gioitinh = DT.Gioitinh;
                    item.Hangdaotao = DT.Hangdaotao;
                    //item.HangGPLX = DT.HangGPLX;
                    item.Hoten = DT.Hoten;
                    item.Ketquathi = DT.Ketquathi;

                    item.TenCSDT = DT.TenCSDT;
                    item.MaLop = DT.MaLop;
                    item.TenSo = DT.TenSo;

                    //item.NgaycapGPLX = DT.NgaycapGPLX;
                    //item.NgayhethanGPLX = DT.NgayhethanGPLX;
                    item.Ngaysinh = DT.Ngaysinh;
                    item.Que = DT.Que;

                    item.Vanhoa = DT.Vanhoa;
                    item.CCsupham = DT.CCsupham;
                    item.Cmnd = DT.Cmnd;

                    db.SubmitChanges();

                }
            }
        }
        public void InsertDaotao(Daotao dt)
        {
            using (var db = new DTGVDataContext())
            {
                try
                {
                    db.Daotaos.InsertOnSubmit(dt);
                    db.SubmitChanges();
                }
                catch(Exception ex)
                { }
                

            }
        }
        public void DeleteDaotao(Daotao dt)
        {
            using (var db = new DTGVDataContext())
            {
                var item = db.Daotaos.FirstOrDefault(p => p.GPLX == dt.GPLX);
                if (item != null)
                {
                    db.Daotaos.DeleteOnSubmit(item);
                    db.SubmitChanges();
                }


            }
        }

    }
}