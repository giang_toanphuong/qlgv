﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DTGV.Services;

namespace DTGV
{
    public partial class QuyetdinhDatEdit : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnLuu_Click(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                var _soquyetdinh = Request.QueryString["soquyetdinh"].ToString();
                QuyetdinhDat dt = new QuyetdinhDat();
                dt.Soquyetdinh = int.Parse(_soquyetdinh);
                dt.Ngaylap = bdpNgaylap.SelectedDate;dt.Malop = txtMalop.Text;
                dt.Noidung = txtNoidung.Text;
                dt.Ghichu = txtGhichu.Text;
                QuyetdinhDatService svc = new QuyetdinhDatService();
                svc.UpdateQuyetdinh(dt);
            }
        }

        protected void btHuy1_Click(object sender, EventArgs e)
        {
            Response.Redirect("QuyetdinhDat.aspx");
        }
    }
}