﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DTGV.Services;

namespace DTGV
{
    public partial class QuyetdinhDatAddDaotao : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                using (var db = new DTGVDataContext())
                {
                    var listHoso = db.Daotaos.Where(p => p.Ketquathi == "Đạt");
                    int i = listHoso.Count();
                    if (i > 0)
                    {
                        drgHosoDat.DataSource = listHoso;
                        drgHosoDat.DataBind();
                    }
                    else
                    {
                        lblMessage.Text = "KHÔNG CÓ GIÁO VIÊN ĐẠT";
                    }
                }
            }
        }
        private int ThamNien(DateTime ngaycapGPLX)
        {
            int _ThamNien = 0;
            int _year = (DateTime.Now.Year - ngaycapGPLX.Year);
            if (DateTime.Now.Month < ngaycapGPLX.Month)
            {
                _year = _year - 1;
            }
            else
            {
                if (DateTime.Now.Month == ngaycapGPLX.Month)
                {
                    if (DateTime.Now.Day < ngaycapGPLX.Day)
                    {
                        _year = _year - 1;
                    }
                }

            }
            _ThamNien = _year;
            return _ThamNien;
        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            using (var db = new DTGVDataContext())
            {
                var _soquyetdinh = Request.QueryString["_soquyetdinh"].ToString();
                DateTime _ngaylapQD =DateTime.Parse(Request.QueryString["ngaylap"].ToString());
                foreach (GridViewRow row in drgHosoDat.Rows)
                {

                    bool isChecked = ((CheckBox)row.FindControl("chkChon")).Checked;

                    if (isChecked==false)
                    {//add giao vien
                        string _cmnd = (row.FindControl("lblCmnd") as Label).Text;
                        var _giaovien = db.Giaoviens.FirstOrDefault(p => p.Cmnd == _cmnd);
                        var _daotao= db.Daotaos.FirstOrDefault(p => p.Cmnd == _cmnd);
                        var itemQuyetdinhKtra = db.QuyetdinhKTras.FirstOrDefault(p => p.Malop == _daotao.MaLop);
                        //them moi giao vien hoan thanh tap huan
                        _giaovien.CCSupham = _daotao.CCsupham;
                        _giaovien.Cmnd = _daotao.Cmnd;
                        _giaovien.Ghichu = _daotao.Ghichu;
                        _giaovien.Gioitinh = _daotao.Gioitinh;
                        _giaovien.GPLX = _daotao.GPLX;
                        //_giaovien.HangGPLX = _daotao.HangGPLX;
                        _giaovien.Hinhanh = _daotao.Hinhanh;
                        _giaovien.Hoten = _daotao.Hoten;
                        _giaovien.TenCSDT = _daotao.TenCSDT;
                        //_giaovien.Ngaycap = _daotao.NgaycapGPLX;
                        //_giaovien.Ngayhethan = _daotao.NgayhethanGPLX;
                        _giaovien.Ngaysinh = _daotao.Ngaysinh;
                        _giaovien.Que = _daotao.Que;
                        _giaovien.Vanhoa = _daotao.Vanhoa;
                        _giaovien.soquyetdinhDat = _soquyetdinh;
                        _giaovien.soquyetdinhKtra = itemQuyetdinhKtra.Soquyetdinh.ToString();
                        //_giaovien.Thamnien = ThamNien(_ngaylapQD);
                        _giaovien.Ngaybatdau = DateTime.Now;
                        GiaovienService svc = new GiaovienService();
                        svc.InsertGiaovien(_giaovien);

                    }


                }
                db.SubmitChanges();
            }
            Response.Redirect("QuyetdinhDat.aspx");
        }

        protected void btnCanel_Click(object sender, EventArgs e)
        {
            Response.Redirect("QuyetdinhDat.aspx");
        }
        protected void drgHosoDat_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            drgHosoDat.PageIndex = e.NewPageIndex;
            drgHosoDat.DataBind();
        }
    }
}