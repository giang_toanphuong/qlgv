﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace DTGV
{
    public partial class LopAddDaotao : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                using (var db = new DTGVDataContext())
                {
                    var listHoso = db.Daotaos.Where(p => p.MaLop == null || p.MaLop == "");
                    int i = listHoso.Count();
                    if (i > 0)
                    {
                        drgHoso.DataSource = listHoso;
                        drgHoso.DataBind();
                    }
                    else
                    {
                        lblMessage.Text = "KHÔNG CÓ GIÁO VIÊN CẦN THAM GIA ĐÀO TẠO";
                    }
                }
            }
        }


        protected void btnSave_Click(object sender, EventArgs e)
        {


            using (var db = new DTGVDataContext())
            {
                var _malop = Request.QueryString["malop"].ToString();
                foreach (GridViewRow row in drgHoso.Rows)
                {

                    bool isChecked = ((CheckBox)row.FindControl("chkChon")).Checked;

                    if (isChecked)
                    {
                        string _cmnd = (row.FindControl("lblCmnd") as Label).Text;
                        var item = db.Daotaos.FirstOrDefault(p => p.Cmnd == _cmnd);
                        item.MaLop = _malop;

                    }


                }
                db.SubmitChanges();
            }
            Response.Redirect("Lop.aspx");

        }
        protected void drgHoso_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            drgHoso.PageIndex = e.NewPageIndex;
            drgHoso.DataBind();
        }
        protected void btnCanel_Click(object sender, EventArgs e)
        {
            Response.Redirect("Lop.aspx");
        }

        
    }
}