﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using DTGV.Services;

using System.Data;
using System.Drawing;
using System.Data.SqlClient;
using System.Configuration;

namespace DTGV
{
    public partial class Chamthi : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                using (var db = new DTGVDataContext())
                {
                    List<Daotao> listds = new List<Daotao>();
                    foreach (var item in db.Daotaos)
                    {
                        if ((item.Ketquathi == null || item.Ketquathi == "")&&(item.MaLop!=null))
                        {
                            listds.Add(item);
                        }

                    }
                    //var listds = db.Daotaos.Where(p => p.Ketquathi == null || p.Ketquathi == "");
                    int i = listds.Count();
                    if (i > 0)
                    {
                        Label1.Text = "Danh sách chấm thi";
                        Label2.Text = "(Mặc định đạt)";
                        drgtest.DataSource = listds;
                        drgtest.DataBind();
                    }
                    else
                    {
                        lblMessage.Text = "KHÔNG CÓ GIÁO VIÊN CẦN CHẤM THI";
                    }


                }
                //var listHoso = new DaotaoService().DanhSachdaotao();

            }
        }
        protected void drgtest_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            drgtest.PageIndex = e.NewPageIndex;
            drgtest.DataBind();
        }
        protected void btnLuuDL_Click(object sender, EventArgs e)
        {
            using (var db = new DTGVDataContext())
            {
                foreach (GridViewRow row in drgtest.Rows)
                {

                    bool _vangthiChecked = ((CheckBox)row.FindControl("chkVangthi")).Checked;
                    if (_vangthiChecked)
                    {
                        string _cmnd = (row.FindControl("lblCmnd") as Label).Text;
                        var item = db.Daotaos.FirstOrDefault(p => p.Cmnd == _cmnd);
                        item.Ketquathi = "Vắng thi";
                    }
                    else
                    {
                        bool _vanbanChecked = ((CheckBox)row.FindControl("chkVanban")).Checked;
                        bool _lythuyetChecked = ((CheckBox)row.FindControl("chkLythuyet")).Checked;
                        bool _thuchanhtronghinhChecked = ((CheckBox)row.FindControl("chkThuchanhtronghinh")).Checked;
                        bool _thuchanhbaohinhChecked = ((CheckBox)row.FindControl("chkThuchanhbaohinh")).Checked;
                        if ((_vanbanChecked == false) && (_lythuyetChecked == false) && (_thuchanhbaohinhChecked == false) && (_thuchanhtronghinhChecked == false))
                        {
                            string _cmnd = (row.FindControl("lblCmnd") as Label).Text;
                            var item = db.Daotaos.FirstOrDefault(p => p.Cmnd == _cmnd);
                            item.Ketquathi = "Đạt";
                        }
                        else
                        {
                            string _cmnd = (row.FindControl("lblCmnd") as Label).Text;
                            var item = db.Daotaos.FirstOrDefault(p => p.Cmnd == _cmnd);
                            item.Ketquathi = "Trượt";
                        }
                    }


                }
                db.SubmitChanges();
                var listChamthi = db.Daotaos.Where(p => p.Ketquathi == null || p.Ketquathi == ""&&p.MaLop!=null);
                drgtest.DataSource = listChamthi;
                drgtest.DataBind();
                if(drgInDS.Rows.Count==0)
                {
                    Label1.Text = "";
                    Label2.Text = "";
                }

            }
        }

        protected void btnTimKiem_Click(object sender, EventArgs e)
        {
            using (var db = new DTGVDataContext())
            {
                if ((string.IsNullOrEmpty(txtMalop.Text)) && (string.IsNullOrEmpty(txtTenlop.Text)))
                {
                    var listChamthi = db.Daotaos.Where(p => p.Ketquathi == null || p.Ketquathi == "");
                    drgtest.DataSource = listChamthi;
                    drgtest.DataBind();

                }
                else
                {
                    if (string.IsNullOrEmpty(txtMalop.Text))
                    {
                        var lops = from p in db.Lops select p;
                        lops = lops.Where(p => p.Tenlop == txtTenlop.Text);
                        string _malop = "";
                        foreach (Lop _lop in lops)
                        {
                            _malop = _lop.Malop;

                        }
                        var daotaos = from p in db.Daotaos select p;
                        daotaos = daotaos.Where(p => p.MaLop == _malop && (p.Ketquathi == null || p.Ketquathi == ""));
                        drgtest.DataSource = daotaos;
                        drgtest.DataBind();

                    }
                    else
                    {
                        var daotaos = from p in db.Daotaos select p;
                        daotaos = daotaos.Where(p => p.MaLop == txtMalop.Text && (p.Ketquathi == null || p.Ketquathi == ""));
                        drgtest.DataSource = daotaos;
                        drgtest.DataBind();

                    }


                }
            }
        }

        protected void btnDSDo_Click(object sender, EventArgs e)
        {
            txtInDS.Text = "Danh sách giáo viên đỗ";
            using (var db = new DTGVDataContext())
            {
                var _daotao = from p in db.Daotaos select p;
                _daotao = _daotao.Where(p => p.Ketquathi == "Đạt");
                drgInDS.DataSource = _daotao;
                drgInDS.DataBind();
            }
        }

        protected void btnDSTruot_Click(object sender, EventArgs e)
        {
            txtInDS.Text = "Danh sách giáo viên trượt";
            using (var db = new DTGVDataContext())
            {
                var _daotao = from p in db.Daotaos select p;
                _daotao = _daotao.Where(p => p.Ketquathi == "Trượt");
                drgInDS.DataSource = _daotao;
                drgInDS.DataBind();
            }
        }

        protected void btnDSvangthi_Click(object sender, EventArgs e)
        {
            txtInDS.Text = "Danh sách giáo viên vắng thi";
            using (var db = new DTGVDataContext())
            {
                var _daotao = from p in db.Daotaos select p;
                _daotao = _daotao.Where(p => p.Ketquathi == "Vắng thi");
                drgInDS.DataSource = _daotao;
                drgInDS.DataBind();
            }
        }
        protected void drgInDS_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            drgInDS.PageIndex = e.NewPageIndex;
            drgInDS.DataBind();
        }

        protected void btnXuatDL_Click(object sender, EventArgs e)
        {
           
            Response.ClearContent();
            Response.AppendHeader("content-disposition", "attachment;filename=EmployeeDetails.xls");
            Response.ContentType = "application/excel";

            StringWriter stringwriter = new StringWriter();
            HtmlTextWriter htmtextwriter = new HtmlTextWriter(stringwriter);
            htmtextwriter.WriteLine("<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\">");
            drgInDS.HeaderRow.Style.Add("background-color", "#ffffff");

            foreach (TableCell tableCell in drgInDS.HeaderRow.Cells)
            {
                tableCell.Style["background-color"] = "#ffffff";
            }

            foreach (GridViewRow gridviewrow in drgInDS.Rows)
            {
                gridviewrow.BackColor = System.Drawing.Color.White;
                foreach (TableCell gridviewrowtablecell in gridviewrow.Cells)
                {
                    gridviewrowtablecell.Style["background-color"] = "#ffffff";
                }
            }

            drgInDS.RenderControl(htmtextwriter);
            Response.Write(stringwriter.ToString());
            Response.End();
        }
        public override void  VerifyRenderingInServerForm(Control control)
        {
            return;
        }
    }
}