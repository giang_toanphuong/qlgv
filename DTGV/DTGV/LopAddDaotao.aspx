﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="LopAddDaotao.aspx.cs" Inherits="DTGV.LopAddDaotao" %>

<%@ Register Assembly="BasicFrame.WebControls.BasicDatePicker" Namespace="BasicFrame.WebControls" TagPrefix="BDP" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="col-sm-8 text-left">
        <h3>Danh sách hồ sơ đào tạo</h3>
    </div>
    <asp:Label ID="lblMessage" runat="server" Text="Label"></asp:Label>
    <div class="table-responsive">


        <asp:GridView ID="drgHoso" AllowPaging="True" PageSize="10" runat="server" CssClass="table table-striped" AutoGenerateColumns="False" OnPageIndexChanging="drgHoso_PageIndexChanging">
            <Columns>
                <asp:TemplateField HeaderText="Họ tên">
                    <ItemTemplate><%# Eval("Hoten") %></ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField HeaderText="CMND">
                    <ItemTemplate>
                        <asp:Label ID="lblCmnd" runat="server" Text='<%# Eval("Cmnd") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Số GPLX">

                    <ItemTemplate>
                        <asp:Label ID="lblGPLX" runat="server" Text='<%# Eval("GPLX") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Hạng GPLX">
                    <ItemTemplate><%# Eval("HangGPLX") %></ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Hạng đào tạo">
                    <ItemTemplate><%# Eval("Hangdaotao") %></ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Chọn">
                    <ItemTemplate>
                        <asp:CheckBox ID="chkChon" runat="server" />
                    </ItemTemplate>
                </asp:TemplateField>

            </Columns>
        </asp:GridView>


    </div>

    <div class="form-group">
        
        <div class="col-md-2 text-right">
            <asp:Button ID="btnSave" runat="server" CssClass="btn btn-primary" Text="Lưu DL" OnClick="btnSave_Click" />
        </div>
        <div class="col-md-1 text-right">
            <asp:Button ID="btnCanel" runat="server" CssClass="btn btn-primary" Text="Thoát" OnClick="btnCanel_Click" />
        </div>

    </div>

    
</asp:Content>
