﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="DaotaoView.aspx.cs" Inherits="DTGV.Daotao_View" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3>Thông tin hồ sơ cá nhân</h3>
        </div>
      
        <div class="panel-body panel-body-nopadding">
              <div class="danger"><asp:Label ID="lblErr" runat="server"></asp:Label></div>
            <div class="form-horizontal form-striped">
                <br />
                <div class="form-group">
                    <label class="col-sm-2 control-label text-right">Họ và tên</label>
                    <div class="col-sm-4">
                        <asp:TextBox ID="txtHoten" CssClass="form-control" runat ="server"  ></asp:TextBox>
                    </div>
                    <label class="col-sm-2 control-label text-right">Ngày sinh</label>
                    <div class="col-sm-4">
                        <asp:TextBox runat="server" ID="txtNgaysinh" CssClass="form-control"></asp:TextBox>
                        
                        
                    </div>

                </div>

                <div class="form-group">
                    <label class="col-sm-2 control-label text-right">Giới tính</label>
                    <div class="col-sm-4">
                        <asp:TextBox runat="server" ID="txtGioitinh" CssClass="form-control"></asp:TextBox>
                    </div>

                    <label class="col-sm-2 control-label text-right">Số CMND</label>
                    <div class="col-sm-4">
                        <asp:TextBox runat="server" ID="txtCMND" CssClass="form-control"></asp:TextBox>
                    </div>

                </div>

                <%--<%# Eval("NgaycapGPLX") %><%# Eval("NgayhethanGPLX") %>--%>
                <div class="form-group">
                    <label class="col-sm-2 control-label text-right">Số GPLX</label>
                    <div class="col-sm-4">
                        <asp:TextBox runat="server" ID="txtGPLX" CssClass="form-control"></asp:TextBox>
                    </div>
                    <label class="col-sm-2 control-label text-right">Hạng GPLX</label>
                    <div class="col-sm-4">
                        <asp:TextBox runat="server" ID="txtHangGPLX" CssClass="form-control"></asp:TextBox>
                        
                    </div>

                </div>
                 <div class="form-group">
                    <label class="col-sm-2 control-label text-right">Hạng đào tạo</label>
                    <div class="col-sm-4">
                        <asp:TextBox runat="server" ID="txtHangDT" CssClass="form-control"></asp:TextBox>
                    </div>
                    <label class="col-sm-2 control-label text-right">Trình độ</label>
                    <div class="col-sm-4">
                        <asp:TextBox runat="server" ID="txtvanhoa" CssClass="form-control"></asp:TextBox>
                    </div>

                </div>

                <div class="form-group">
                    <label class="col-sm-2 control-label text-right">Ngày cấp</label>
                    <div class="col-sm-4">
                        <%--<asp:TextBox runat="server" ID="txtNgaycap" CssClass="form-control"></asp:TextBox>--%>
                        <asp:TextBox runat="server" ID="txtNgaycap" CssClass="form-control"></asp:TextBox>
                    </div>
                    <label class="col-sm-2 control-label text-right">Ngày hết hạn</label>
                    <div class="col-sm-4">
                        <asp:TextBox runat="server" ID="txtNgayhethan" CssClass="form-control"></asp:TextBox>
                    </div>

                </div>

                <div class="form-group">
                    <label class="col-sm-2 control-label text-right">Quê quán</label>
                    <div class="col-sm-4">
                        <asp:TextBox runat="server" ID="txtQue" CssClass="form-control"></asp:TextBox>
                    </div>
                    <label class="col-sm-2 control-label text-right">CC Sư phạm</label>
                    <div class="col-sm-4">
                        <asp:TextBox runat="server" ID="txtCCSP" CssClass="form-control"></asp:TextBox>
                    </div>

                </div>

                <div class="form-group">
                    <label class="col-sm-2 control-label text-right">Tên Sở</label>
                    <div class="col-sm-4">
                        <asp:TextBox runat="server" ID="txtTenSo" CssClass="form-control"></asp:TextBox>
                    </div>
                    <label class="col-sm-2 control-label text-right">Tên CSĐT</label>
                    <div class="col-sm-4">
                        <asp:TextBox runat="server" ID="txtTenCSDT" CssClass="form-control"></asp:TextBox>
                    </div>

                </div>

                <div class="form-group">
                    <label class="col-sm-2 control-label text-right">Mã lớp</label>
                    <div class="col-sm-4">
                        <asp:TextBox runat="server" ID="txtMaLop" CssClass="form-control"></asp:TextBox>
                    </div>
                    <label class="col-sm-2 control-label text-right">Ghi chú</label>
                    <div class="col-sm-4">
                        <asp:TextBox runat="server" ID="txtGhichu" CssClass="form-control"></asp:TextBox>
                    </div>
                </div>

                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label text-right">Hình ảnh</label>
                    <br />
                    <div class="col-sm-4">
                        <asp:Image ID="Image1" runat="server" Width="300" Height="200" />

                    </div>
                    
                    

                <div class="form-group">
                    <%--<div class="col-md-10 text-right">
    
                        <asp:Button ID="btLuu" runat="server" CssClass="btn btn-primary" Text="Lưu" OnClick="btLuu_Click" />
                    </div>--%>
                    <div class="col-md-11 text-right">
                        <asp:Button ID="btHuy1" runat="server" CssClass="btn btn-primary" Text="Thoát" OnClick="btHuy1_Click"/>
                    </div>

                </div>
                <br />
            </div>
        </div>
    </div>
</asp:Content>
