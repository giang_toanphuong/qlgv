﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Daotao.aspx.cs" Inherits="DTGV.Daotao1" %>

<%@ Register Assembly="BasicFrame.WebControls.BasicDatePicker" Namespace="BasicFrame.WebControls" TagPrefix="BDP" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
     <div class="panel panel-default">
        <div class="panel-heading">
            <h3>Thông tin tìm kiếm</h3>
        </div>
        <div class="panel-body panel-body-nopadding">
            <div class="form-horizontal form-striped">
                <br />
                <div class="form-group">
                    <label class="col-sm-2 control-label text-right">Họ và tên</label>
                    <div class="col-sm-4">
                        <asp:TextBox runat="server" ID="txtHotenTK" CssClass="form-control"></asp:TextBox>
                    </div>
                    <label class="col-sm-2 control-label text-right">Số CMND</label>
                    <div class="col-sm-4">
                        <asp:TextBox runat="server" ID="txtCMNDTK" CssClass="form-control"></asp:TextBox>
                    </div>

                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label text-right">Số GPLX</label>
                    <div class="col-sm-4">
                        <asp:TextBox runat="server" ID="txtGPLXTK" CssClass="form-control"></asp:TextBox>
                    </div>
                    <label class="col-sm-2 control-label text-right">Trình độ</label>
                    <div class="col-sm-4">
                        <asp:TextBox runat="server" ID="txtTrinhdoTK" CssClass="form-control"></asp:TextBox>
                    </div>

                </div>
                <div class="form-group">
                    <div class="col-md-10 text-right">
                        <asp:Button ID="btnSearch" runat="server" Text="Tìm kiếm" CssClass=" btn btn-primary" OnClick="btnSearch_Click" />
                    </div>

                </div>
                <br />
            </div>
        </div>
    </div>
    <script>
        function btnClick() {
            //    button click
            $("#btnSearch").button().click(function () {
                alert("Tìm kiếm");
            });
        }
    </script>

    <div class="col-sm-8 text-left">
        <h3>Danh sách hồ sơ đào tạo</h3>
    </div>

    <div class="table-responsive">

        <asp:GridView ID="drgDaotao" AllowPaging="True" PageSize="10" runat="server" CssClass="table table-striped" AutoGenerateColumns="False" OnPageIndexChanging="drgDaotao_PageIndexChanging">
            <Columns>
                <asp:TemplateField HeaderText="Họ tên">
                    <ItemTemplate><%# Eval("Hoten") %></ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Giới tính">
                    <ItemTemplate><%# Eval("Gioitinh") %></ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Ngày sinh">
                    <ItemTemplate><%# Eval("Ngaysinh") %></ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="CMND">
                    <ItemTemplate><%# Eval("Cmnd") %></ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Số GPLX">
                    <ItemTemplate><%# Eval("GPLX") %></ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Ngày cấp">
                    <ItemTemplate><%# Eval("NgaycapGPLX") %></ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Cơ sở đào tạo">
                    <ItemTemplate><%# Eval("TenCSDT") %></ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField HeaderText="Hiển thị">
                    <ItemTemplate>
                        <asp:LinkButton runat="server" Text="Hiển Thị" ID="btnView" CommandArgument='<%# Eval("cmnd")%> ' OnCommand="btnView_Command"></asp:LinkButton>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
        </asp:GridView>
    </div>
    <div class=" form-group">
        <div class="col-md-10 text-right">
            <asp:Button ID="btnDaotaoAdd" runat="server" Text="Thêm mới Hồ sơ" CssClass="btn btn-primary" OnClick="btnDaotaoAdd_Click"  />

        </div>

    </div>


</asp:Content>
