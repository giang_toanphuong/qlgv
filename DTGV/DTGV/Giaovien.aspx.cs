﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DTGV.Services;
namespace DTGV
{
    public partial class Giaovien1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            var listGV = new GiaovienService().DanhSachGiaoVien();
            drgGV.DataSource = listGV;
            drgGV.DataBind();
        }
        protected void drgGV_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            drgGV.PageIndex = e.NewPageIndex;
            drgGV.DataBind();
        }
        private bool validData()
        {
            if (string.IsNullOrEmpty(txtHoten.Text))
            {
                lblErr.Text = "Nhập họ và tên";
                return false;
            }
            if (string.IsNullOrEmpty(txtCmnd.Text))
            {
                lblErr.Text = "Nhập số chứng minh nhân dân";
                return false;
            }
            if (string.IsNullOrEmpty(txtGPLX.Text))
            {
                lblErr.Text = "Nhập số giấy phép lái xe";
                return false;
            }

            if (string.IsNullOrEmpty(txtVanhoa.Text))
            {
                lblErr.Text = "Nhập lại quê";
                return false;
            }


            return true;
        }
        protected void btnView_Command(object sender, CommandEventArgs e)
        {

            var cmnd = e.CommandArgument.ToString();

            Response.Redirect("GiaovienView.aspx?cmnd=" + cmnd);



        }

        protected void btnTimKiem_Click(object sender, EventArgs e)
        {
            var listGV = new GiaovienService().TimKiem(txtHoten.Text, txtCmnd.Text, txtGPLX.Text, txtVanhoa.Text);
            drgGV.DataSource = listGV;
            drgGV.DataBind();
        }
    }
}