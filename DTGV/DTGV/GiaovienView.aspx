﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="GiaovienView.aspx.cs" Inherits="DTGV.GiaovienView" %>
<%@ Register Assembly="BasicFrame.WebControls.BasicDatePicker" Namespace="BasicFrame.WebControls" TagPrefix="BDP" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3>Thông tin hồ sơ cá nhân</h3>
        </div>

        <div class="panel-body panel-body-nopadding">
            <div class="danger">
                <asp:Label ID="lblErr" runat="server"></asp:Label>
            </div>
            <div class="form-horizontal form-striped">
                <br />
                <div class="form-group">
                    <label class="col-sm-2 control-label text-right">Họ và tên</label>
                    <div class="col-sm-4">
                        <asp:TextBox ID="txtHoten" CssClass="form-control" runat="server"></asp:TextBox>
                    </div>
                    <label class="col-sm-2 control-label text-right">Ngày sinh</label>
                    <div class="col-sm-4">
                        <BDP:BDPLite ID="bdpNgaysinh1" CssClass="form-control" DateFormat="d" runat="server">
                        </BDP:BDPLite>

                    </div>

                </div>

                <div class="form-group">
                    <label class="col-sm-2 control-label text-right">Giới tính</label>
                    <div class="col-sm-4">
                        <asp:TextBox runat="server" ID="txtGioitinh" CssClass="form-control"></asp:TextBox>
                    </div>
                    <label class="col-sm-2 control-label text-right">Số CMND</label>
                    <div class="col-sm-4">
                        <asp:TextBox runat="server" ID="txtCMND" CssClass="form-control"></asp:TextBox>
                    </div>

                </div>


                <div class="form-group">
                    <label class="col-sm-2 control-label text-right">Số GPLX</label>
                    <div class="col-sm-4">
                        <asp:TextBox runat="server" ID="txtGPLX" CssClass="form-control"></asp:TextBox>
                    </div>
                    <label class="col-sm-2 control-label text-right">Hạng GPLX</label>
                    <div class="col-sm-4">
                        <asp:TextBox runat="server" ID="txtHangGPLX" CssClass="form-control"></asp:TextBox>
                    </div>

                </div>


                <div class="form-group">
                    <label class="col-sm-2 control-label text-right">Ngày cấp</label>
                    <div class="col-sm-4">

                        <BDP:BDPLite CssClass="form-control" DateFormat="d" ID="bdpNgaycap" runat="server">
                        </BDP:BDPLite>
                    </div>
                    <label class="col-sm-2 control-label text-right">Ngày hết hạn</label>
                    <div class="col-sm-4">
                        <BDP:BDPLite CssClass="form-control" DateFormat="d" ID="bdpNgayhethan" runat="server" />
                    </div>

                </div>

                <div class="form-group">
                    <label class="col-sm-2 control-label text-right">Quê quán</label>
                    <div class="col-sm-4">
                        <asp:TextBox runat="server" ID="txtQue" CssClass="form-control"></asp:TextBox>
                    </div>
                    <label class="col-sm-2 control-label text-right">CC Sư phạm</label>
                    <div class="col-sm-4">
                        <asp:TextBox runat="server" ID="txtCCSP" CssClass="form-control"></asp:TextBox>
                    </div>

                </div>

                <div class="form-group">
                    
                    <label class="col-sm-2 control-label text-right">Mã CSĐT</label>
                    <div class="col-sm-4">
                        <asp:TextBox runat="server" ID="txtMaCSDT" CssClass="form-control"></asp:TextBox>
                    </div>
                    <label class="col-sm-2 control-label text-right">Văn hoá</label>
                    <div class="col-sm-4">
                        <asp:TextBox runat="server" ID="txtVanhoa" CssClass="form-control"></asp:TextBox>
                    </div>

                </div>


                <div class="form-group">
                    <label class="col-sm-2 control-label text-right">Hình ảnh</label>
                    <br />
                    <div class="col-sm-4">
                        <asp:Image ID="Image1" runat="server" Width="300" Height="200" />

                    </div>

                    <label class="col-sm-2 control-label text-right">Ghi chú</label>
                    <div class="col-sm-4">
                        <asp:TextBox runat="server" ID="txtGhichu" CssClass="form-control"></asp:TextBox>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-md-11 text-right">
                        <asp:Button ID="btHuy1" runat="server" CssClass="btn btn-primary" Text="Huỷ" OnClick="btHuy1_Click"  />
                    </div>

                </div>
                <br />
            </div>
        </div>
    </div>
</asp:Content>
